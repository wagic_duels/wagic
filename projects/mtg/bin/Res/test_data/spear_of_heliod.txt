[card]
name=Spear of Heliod
auto=lord(creature|mybattlefield) 1/1
auto={1}{W}{W}{T}:target(creature[controllerdamager]|battlefield) destroy
text=Creatures you control get +1/+1. -- {1}{W}{W}, {T}: Destroy target creature that dealt damage to you this turn.
mana={1}{W}{W}
type=Legendary Enchantment Artifact
[/card]