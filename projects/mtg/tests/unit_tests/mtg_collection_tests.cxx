#include <MTGCollection.h>
#include <MTGDeck.h>

#include <JFileSystem.h>

#include <wsl/utility.hxx>

#include <catch.hpp>

int JGEGetTime() { return 0; }
bool JGEToggleFullscreen() { return true; }
void JGECreateDefaultBindings() {}

TEST_CASE("test collection basics", "[mtg][cards]")
{
  const std::string work_root_dir(TEST_RES_ROOT_DIR);
  const std::string sys_dir(work_root_dir + "/Res");
  const std::string user_dir(work_root_dir + "/User");
  const std::string test_data_dir{"test_data/"};

  auto cleanup = wsl::finally([]() {
    JFileSystem::Destroy();
    MTGCollection()->unloadAll();
    setlist.clear();
  });

  JFileSystem::init(user_dir, sys_dir);

  SECTION("database is empty bevor loading")
  {
    REQUIRE(MTGCollection()->database_size() == 0u);
    REQUIRE(MTGCollection()->collection_size() == 0u);
  }

  SECTION("primitives size increase after card loading")
  {
    const std::string test_file(test_data_dir + "spear_of_heliod.txt");
    MTGCollection()->load(test_file);
    REQUIRE(MTGCollection()->primitives_size() == 1u);
    REQUIRE(MTGCollection()->collection_size() == 0u);
    REQUIRE(MTGCollection()->database_size() == 0u);
  }

  SECTION("load only set file")
  {
    const std::string set_name("THS");
    const std::string test_file(test_data_dir + "theros_set.txt");
    MTGCollection()->load(test_file, set_name);
    REQUIRE(MTGCollection()->primitives_size() == 0u);
    REQUIRE(MTGCollection()->collection_size() == 0u);
    REQUIRE(MTGCollection()->database_size() == 0u);
    REQUIRE(setlist.size() == 1u);
  }

  SECTION("load card and set")
  {
    const std::string set_name("THS");

    const std::string test_file(test_data_dir + "spear_of_heliod.txt");
    MTGCollection()->load(test_file);

    const std::string set_file(test_data_dir + "theros_set.txt");
    MTGCollection()->load(set_file, set_name);

    REQUIRE(MTGCollection()->primitives_size() == 1u);
    REQUIRE(MTGCollection()->collection_size() == 1u);
    REQUIRE(MTGCollection()->database_size() == 1u);
    REQUIRE(setlist.size() == 1u);
  }
}