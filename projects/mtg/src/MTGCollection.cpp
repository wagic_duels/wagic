#include "MTGCollection.h"

#include "AbilityParser.h"
#include "CardPrimitive.h"
#include "GameOptions.h"
#include "MTGCard.h"
#include "MTGDeck.h"
#include "Translate.h"

#include <config.h>
#include <utils.h>

#include <JFileSystem.h>

#include <algorithm>

#include <wsl/debug.hxx>
#include <wsl/log.hxx>

static inline int getGrade(int v)
{
  switch (v)
  {
  case 'P':
  case 'p':
    return Constants::GRADE_SUPPORTED;
  case 'R':
  case 'r':
    return Constants::GRADE_BORDERLINE;
  case 'O':
  case 'o':
    return Constants::GRADE_UNOFFICIAL;
  case 'A':
  case 'a':
    return Constants::GRADE_CRAPPY;
  case 'S':
  case 's':
    return Constants::GRADE_UNSUPPORTED;
  case 'N':
  case 'n':
    return Constants::GRADE_DANGEROUS;
  }
  return 0;
}

// MTGAllCards
int MTGAllCards::processConfLine(string& s, MTGCard* card, CardPrimitive* primitive)
{
  if ('#' == s[0])
    return 1; // a comment shouldn't be treated as an error condition

  size_t del_pos = s.find_first_of('=');
  if (del_pos == string::npos || 0 == del_pos)
    return 0;

  s[del_pos] = '\0';
  const string key = s.substr(0, del_pos);
  const string val = s.substr(del_pos + 1);

  // WSL_DEBUG() << "key:" << key << " value: " << val;
  switch (key[0])
  {
  case 'a':
    if (key == "aicode") // replacement code for AI. for reveal:number basic version only
    {
      if (!primitive)
        primitive = NEW CardPrimitive();
      primitive->setAICustomCode(val);
    }
    else if (key == "auto")
    {
      if (!primitive)
        primitive = NEW CardPrimitive();
      primitive->addMagicText(val);
    }
    else if (StartsWith(key, "auto"))
    {
      if (!primitive)
        primitive = NEW CardPrimitive();
      primitive->addMagicText(val, key.substr(4));
    }
    else if (key == "alias")
    {
      if (!primitive)
        primitive = NEW CardPrimitive();
      primitive->alias = atoi(val.c_str());
    }
    else if (key == "abilities")
    {
      if (!primitive)
        primitive = NEW CardPrimitive();
      string value = val;
      // Specific Abilities
      std::transform(value.begin(), value.end(), value.begin(), ::tolower);
      vector<string> values = split(value, ',');
      for (size_t values_i = 0; values_i < values.size(); ++values_i)
      {

        for (int j = Constants::NB_BASIC_ABILITIES - 1; j >= 0; --j)
        {
          if (values[values_i].find(Constants::MTGBasicAbilities[j]) != string::npos)
          {
            primitive->basicAbilities[j] = 1;
            break;
          }
        }
      }
    }
    if (key == "anyzone")
    {
      if (!primitive)
        primitive = NEW CardPrimitive();
      primitive->addMagicText(val, "hand");
      primitive->addMagicText(val, "library");
      primitive->addMagicText(val, "graveyard");
      primitive->addMagicText(val, "stack");
      primitive->addMagicText(val, "exile");
      primitive->addMagicText(val);
    }
    break;

  case 'b': // buyback/Bestow
    if (!primitive)
      primitive = NEW CardPrimitive();
    if (key[1] == 'e' && key[2] == 's')
    { // bestow
      if (!primitive)
        primitive = NEW CardPrimitive();
      if (ManaCost* cost = primitive->getManaCost())
      {
        string value = val;
        std::transform(value.begin(), value.end(), value.begin(), ::tolower);
        cost->setBestow(ManaCost::parseManaCost(value));
      }
    }
    else // buyback
        if (ManaCost* cost = primitive->getManaCost())
    {
      string value = val;
      std::transform(value.begin(), value.end(), value.begin(), ::tolower);
      cost->setBuyback(ManaCost::parseManaCost(value));
    }
    break;

  case 'c': // crew ability
    if (key == "crewbonus")
    {
      if (!primitive)
        primitive = NEW CardPrimitive();
      {
        primitive->setCrewAbility(val);
        break;
      }
    }
    else if (!primitive)
      primitive = NEW CardPrimitive();
    { // color
      string value = val;
      std::transform(value.begin(), value.end(), value.begin(), ::tolower);
      vector<string> values = split(value, ',');
      int removeAllOthers = 1;
      for (size_t values_i = 0; values_i < values.size(); ++values_i)
      {
        primitive->setColor(values[values_i], removeAllOthers);
        removeAllOthers = 0;
      }
      break;
    }
  case 'd': // double faced card /dredge
    if (key == "doublefaced")
    {
      if (!primitive)
        primitive = NEW CardPrimitive();
      {
        primitive->setdoubleFaced(val);
        break;
      }
    }
    else if (!primitive)
      primitive = NEW CardPrimitive();
    {
      string value = val;
      std::transform(value.begin(), value.end(), value.begin(), ::tolower);
      vector<string> values = parseBetween(value, "dredge(", ")");
      if (values.size())
      {
        auto amount = atoi(values[1].c_str());
        primitive->set_dredge_amount(amount);
      }

      break;
    }
  case 'f': // flashback//morph
  {
    if (!primitive)
      primitive = NEW CardPrimitive();
    if (ManaCost* cost = primitive->getManaCost())
    {
      if (s.find("facedown") != string::npos) // morph
      {
        string value = val;
        std::transform(value.begin(), value.end(), value.begin(), ::tolower);
        cost->setMorph(ManaCost::parseManaCost(value));
      }
      else
      {
        string value = val;
        std::transform(value.begin(), value.end(), value.begin(), ::tolower);
        cost->setFlashback(ManaCost::parseManaCost(value));
      }
    }
    break;
  }

  case 'g': // grade
    if (s.size() - del_pos - 1 > 2)
      currentGrade = getGrade(val[2]);
    break;

  case 'i': // id
    if (!card)
      card = NEW MTGCard();
    card->setMTGId(atoi(val.c_str()));
    break;

  case 'k': // kicker
    if (!primitive)
      primitive = NEW CardPrimitive();
    if (ManaCost* cost = primitive->getManaCost())
    {
      string value = val;
      std::transform(value.begin(), value.end(), value.begin(), ::tolower);
      size_t multikick = value.find("multi");
      bool isMultikicker = false;
      if (multikick != string::npos)
      {
        size_t endK = value.find("{", multikick);
        value.erase(multikick, endK - multikick);
        isMultikicker = true;
      }
      cost->setKicker(ManaCost::parseManaCost(value));
      cost->getKicker()->isMulti = isMultikicker;
    }
    break;

  case 'm': // mana
    if (!primitive)
      primitive = NEW CardPrimitive();
    {
      if (key == "modular") // modular
      {
        primitive->setModularValue(val);
      }
      else
      {
        string value = val;
        std::transform(value.begin(), value.end(), value.begin(), ::tolower);
        primitive->setManaCost(value);
      }
    }
    break;

  case 'n': // name
    if (!primitive)
      primitive = NEW CardPrimitive();
    primitive->setName(val);
    break;

  case 'o': // othercost/otherrestriction
    if (!primitive)
      primitive = NEW CardPrimitive();
    if (key[5] == 'r') // otherrestrictions
    {
      string value = val;
      primitive->setOtherRestrictions(value);
    }
    else
    {
      if (ManaCost* cost = primitive->getManaCost())
      {
        string value = val;
        std::transform(value.begin(), value.end(), value.begin(), ::tolower);
        size_t name = value.find("name(");
        string theName = "";
        if (name != string::npos)
        {
          size_t endName = value.find(")", name);
          theName = value.substr(name + 5, endName - name - 5);
          value.erase(name, endName - name + 1);
        }
        cost->setAlternative(ManaCost::parseManaCost(value));
        if (theName.size())
          cost->getAlternative()->alternativeName.append(theName);
      }
    }
    break;

  case 'p':
    if (key == "phasedoutbonus")
    {
      if (!primitive)
        primitive = NEW CardPrimitive();
      {
        primitive->setPhasedOutAbility(val);
        break;
      }
    }
    else if (key[1] == 'r')
    { // primitive
      if (!card)
        card = NEW MTGCard();
      map<string, CardPrimitive*>::iterator it = primitives.find(val);
      if (it != primitives.end())
        card->setPrimitive(it->second);
    }
    else
    { // power
      if (!primitive)
        primitive = NEW CardPrimitive();
      primitive->setPower(atoi(val.c_str()));
    }
    break;

  case 'r':                             // retrace/rarity//restrictions
    if (key[2] == 's' && key[3] == 't') // restrictions
    {
      if (!primitive)
        primitive = NEW CardPrimitive();
      string value = val;
      primitive->setRestrictions(value);
    }
    else if (key[1] == 'e' && key[2] == 't')
    { // retrace
      if (!primitive)
        primitive = NEW CardPrimitive();
      if (ManaCost* cost = primitive->getManaCost())
      {
        string value = val;
        std::transform(value.begin(), value.end(), value.begin(), ::tolower);
        cost->setRetrace(ManaCost::parseManaCost(value));
      }
    }
    else if (s.find("rar") != string::npos)
    { // rarity
      if (!card)
        card = NEW MTGCard();
      card->setRarity(val[0]);
    }
    break;

  case 's': // subtype, suspend
  {
    if (s.find("suspend") != string::npos)
    {
      size_t time = s.find("suspend(");
      size_t end = s.find(")=");
      int suspendTime = atoi(s.substr(time + 8, end - 2).c_str());
      if (!primitive)
        primitive = NEW CardPrimitive();
      if (ManaCost* cost = primitive->getManaCost())
      {
        string value = val;
        std::transform(value.begin(), value.end(), value.begin(), ::tolower);
        cost->setSuspend(ManaCost::parseManaCost(value));
        primitive->set_suspended_time(suspendTime);
      }
    }
    else
    {
      if (!primitive)
        primitive = NEW CardPrimitive();
      vector<string> values = split(val.c_str(), ' ');
      for (size_t values_i = 0; values_i < values.size(); ++values_i)
        primitive->setSubtype(values[values_i]);
    }
    break;
  }

  case 't':
    if (!primitive)
      primitive = NEW CardPrimitive();
    if (key == "target")
    {
      string value = val;
      std::transform(value.begin(), value.end(), value.begin(), ::tolower);
      primitive->spellTargetType = value;
    }
    else if (key == "text")
      primitive->set_text(val);
    else if (key == "type")
    {
      vector<string> values = split(val, ' ');
      for (size_t values_i = 0; values_i < values.size(); ++values_i)
        primitive->setType(values[values_i]);
    }
    else if (key == "toughness")
      primitive->setToughness(atoi(val.c_str()));
    break;

  default:
    if (primitive)
    {
      WSL_ERROR() << "Parsing Error: [" << primitive->getName() << "]" << s;
    }
    else
    {
      WSL_ERROR() << "Parsing Generic Error: " << s;
    }
    break;
  }

  tempPrimitive = primitive;
  tempCard = card;

  return del_pos;
}

void MTGAllCards::loadFolder(const string& infolder, const string& filename)
{
  string folder = infolder;

  // Make sure the base paths finish with a '/' or a '\'
  if (!folder.empty())
  {
    string::iterator c = folder.end(); // userPath.at(userPath.size()-1);
    c--;
    if ((*c != '/') && (*c != '\\'))
      folder += '/';
  }

  vector<string> files = JFileSystem::GetInstance()->scanfolder(folder);

  if (!files.size())
  {
    return;
  }

  for (size_t i = 0; i < files.size(); ++i)
  {
    string afile = folder;
    afile.append(files[i]);

    if (files[i] == "." || files[i] == "..")
      continue;

    if (JFileSystem::GetInstance()->DirExists(afile))
      loadFolder(afile, filename);

    if (!JFileSystem::GetInstance()->FileExists(afile))
      continue;

    if (filename.size())
    {
      if (filename == files[i])
      {
        load(afile.c_str(), folder.c_str());
      }
    }
    else
    {
      load(afile.c_str());
    }
  }
}

int MTGAllCards::load(const string& config_file) { return load(config_file, MTGSets::INTERNAL_SET); }

int MTGAllCards::load(const string& config_file, const string& set_name)
{
  const int set_id = setlist.Add(set_name);
  return load(config_file, set_id);
}

int MTGAllCards::load(const string& config_file, int set_id)
{
  WSL_DEBUG() << "config_file: " << config_file << " set_id: " << set_id;
  const auto allocated_space_before = wsl::get_complete_allocated_memory();
  conf_read_mode = 0;
  MTGSetInfo* si = setlist.getInfo(set_id);

  izfstream file;
  if (!JFileSystem::GetInstance()->openForRead(file, config_file))
  {
    WSL_ERROR() << "error loading: " << config_file;
    return database_size();
  }

  string s;
  int lineNumber = 0;
  while (getline(file, s))
  {
    lineNumber++;
    if (s.empty())
      continue;
    if (s[s.size() - 1] == '\r')
      s.erase(s.size() - 1); // Handle DOS files
    if (s.empty())
      continue;

    if (s.find("#AUTO_DEFINE ") == 0)
    {
      string toAdd = s.substr(13);
      AutoLineMacro::AddMacro(toAdd);
      continue;
    }

    switch (conf_read_mode)
    {
    case MTGAllCards::READ_ANYTHING:
      if (s[0] == '[')
      {
        currentGrade = Constants::GRADE_SUPPORTED; // Default value
        if (s.size() < 2)
        {
          WSL_FATAL() << "Card file incorrect!!";
        }
        else
        {
          conf_read_mode = ('m' == s[1]) ? MTGAllCards::READ_METADATA : MTGAllCards::READ_CARD; // M for metadata.
        }
      }
      else
      {
        // Global grade for file, to avoid reading the entire file if unnnecessary
        if (s[0] == 'g' && s.size() > 8)
        {
          int fileGrade = getGrade(s[8]);
          int maxGrade = options[Options::MAX_GRADE].number;
          if (!maxGrade)
            maxGrade = Constants::GRADE_BORDERLINE; // Default setting for grade is borderline?
          if (fileGrade > maxGrade)
          {
            file.close();
            return database_size();
          }
        }
      }
      continue;
    case MTGAllCards::READ_METADATA:
      if (s[0] == '[' && s[1] == '/')
        conf_read_mode = MTGAllCards::READ_ANYTHING;
      else if (si)
        si->processConfLine(s);
      continue;
    case MTGAllCards::READ_CARD:
      if (s[0] == '[' && s[1] == '/')
      {
        conf_read_mode = MTGAllCards::READ_ANYTHING;
        if (tempPrimitive)
          tempPrimitive = addPrimitive(tempPrimitive, tempCard);
        if (tempCard)
        {
          if (tempPrimitive)
            tempCard->setPrimitive(tempPrimitive);
          addCardToCollection(tempCard, set_id);
        }
        tempCard = NULL;
        tempPrimitive = NULL;
      }
      else
      {
        if (!processConfLine(s, tempCard, tempPrimitive))
          WSL_ERROR() << "BAD Line: \n[" << lineNumber << "]: " << s;
      }
      continue;
    }
  }
  file.close();

  const auto allocated_space_after = wsl::get_complete_allocated_memory();
  WSL_DEBUG() << "additional space used: " << allocated_space_after - allocated_space_before << " bytes";
  return database_size();
}

MTGAllCards* MTGAllCards::instance = NULL;

MTGAllCards::MTGAllCards() : tempCard(nullptr), tempPrimitive(nullptr)
{
  colorsCount.erase(colorsCount.begin(), colorsCount.end());
  for (int i = 0; i < Constants::NB_Colors; i++)
    colorsCount.push_back(0);
}

MTGAllCards::~MTGAllCards()
{
  for (map<int, MTGCard*>::iterator it = collection.begin(); it != collection.end(); it++)
    delete (it->second);
  collection.clear();
  ids.clear();

  for (map<string, CardPrimitive*>::iterator it = primitives.begin(); it != primitives.end(); it++)
    delete (it->second);
  primitives.clear();
}

MTGAllCards* MTGAllCards::getInstance()
{
  if (!instance)
    instance = new MTGAllCards();

  return instance;
}

void MTGAllCards::unloadAll()
{
  if (instance)
  {
    delete instance;
    instance = NULL;
  }
}

int MTGAllCards::randomCardId()
{
  int id = (rand() % ids.size());
  return ids[id];
}

int MTGAllCards::countBySet(int setId)
{
  int result = 0;
  map<int, MTGCard*>::iterator it;

  for (it = collection.begin(); it != collection.end(); it++)
  {
    MTGCard* c = it->second;
    if (c->setId == setId)
    {
      result++;
    }
  }
  return result;
}

// TODO more efficient way ?
int MTGAllCards::countByType(const string& _type)
{
  int type_id = findType(_type);

  int result = 0;
  map<int, MTGCard*>::iterator it;
  for (it = collection.begin(); it != collection.end(); it++)
  {
    MTGCard* c = it->second;
    if (c->data->hasType(type_id))
    {
      result++;
    }
  }
  return result;
}

int MTGAllCards::countByColor(int color)
{
  if (colorsCount[color] == 0)
  {
    for (int i = 0; i < Constants::NB_Colors; i++)
    {
      colorsCount[i] = 0;
    }
    map<int, MTGCard*>::iterator it;
    for (it = collection.begin(); it != collection.end(); it++)
    {
      MTGCard* c = it->second;
      int j = c->data->getColor();

      colorsCount[j]++;
    }
  }
  return colorsCount[color];
}

bool MTGAllCards::addCardToCollection(MTGCard* card, int setId)
{
  card->setId = setId;
  int newId = card->getId();
  if (collection.find(newId) != collection.end())
  {
#if defined(_DEBUG)
    const string cardName = card->data ? card->data->name : card->getImageName();
    const string setName = setId != -1 ? setlist.getInfo(setId)->getName() : "";
    WSL_WARN() << "card id collision! : " << newId << " -> " << cardName << "(" << setName << ")";
#endif
    SAFE_DELETE(card);
    return false;
  }

  // Don't add cards that don't have a primitive
  if (!card->data)
  {
    SAFE_DELETE(card);
    return false;
  }
  ids.push_back(newId);

  collection[newId] = card; // Push card into collection.
  MTGSetInfo* si = setlist.getInfo(setId);
  if (si)
    si->count(card); // Count card in set info

  return true;
}

CardPrimitive* MTGAllCards::addPrimitive(CardPrimitive* primitive, MTGCard* card)
{
  int maxGrade = options[Options::MAX_GRADE].number;
  if (!maxGrade)
    maxGrade = Constants::GRADE_BORDERLINE; // Default setting for grade is borderline?
  if (currentGrade > maxGrade)
  {
    SAFE_DELETE(primitive);
    return NULL;
  }
  string key;
  if (card)
  {
    std::stringstream ss;
    ss << card->getId();
    ss >> key;
  }
  else
    key = primitive->name;
  if (primitives.find(key) != primitives.end())
  {
    WSL_ERROR() << "MTGDECK: primitives conflict: " << key;
    SAFE_DELETE(primitive);
    return NULL;
  }
  // translate cards text
  Translator* t = Translator::GetInstance();
  map<string, string>::iterator it = t->tempValues.find(primitive->name);
  if (it != t->tempValues.end())
  {
    primitive->set_text(it->second);
  }

  // Legacy:
  // For the Deck editor, we need Lands and Artifact to be colors...
  if (primitive->hasType(Subtypes::TYPE_LAND))
    primitive->setColor(Constants::MTG_COLOR_LAND);
  if (primitive->hasType(Subtypes::TYPE_ARTIFACT))
    primitive->setColor(Constants::MTG_COLOR_ARTIFACT);

  primitives[key] = primitive;
  return primitive;
}

MTGCard* MTGAllCards::get_card_from_id(int id) const noexcept
{
  const auto it = collection.find(id);
  if (it != collection.end())
  {
    return (it->second);
  }
  return nullptr;
}

MTGCard* MTGAllCards::get_card_from_idx(size_type idx) const noexcept
{
  if (idx < database_size())
  {
    const auto card_id = id_at(idx);
    return get_card_from_id(card_id);
  }
  return nullptr;
}

#ifdef TESTSUITE
void MTGAllCards::prefetchCardNameCache()
{
  for (auto it = collection.cbegin(); it != collection.cend(); it++)
  {
    MTGCard* c = it->second;

    // Name only
    string cardName = c->data->name;
    std::transform(cardName.begin(), cardName.end(), cardName.begin(), ::tolower);
    mtgCardByNameCache[cardName] = c;

    // Name + set
    int setId = c->setId;
    MTGSetInfo* setInfo = setlist.getInfo(setId);
    if (setInfo)
    {
      string setName = setInfo->getName();
      std::transform(setName.begin(), setName.end(), setName.begin(), ::tolower);
      cardName = cardName + " (" + setName + ")";
      mtgCardByNameCache[cardName] = c;
    }

    // id
    std::stringstream out;
    out << c->getMTGId();
    mtgCardByNameCache[out.str()] = c;
  }
}
#endif

MTGCard* MTGAllCards::getCardByName(string nameDescriptor)
{
  wsl::scoped_lock<wsl::mutex> lock(instance->mMutex);
  if (!nameDescriptor.size())
    return NULL;
  if (nameDescriptor[0] == '#')
    return NULL;

  std::transform(nameDescriptor.begin(), nameDescriptor.end(), nameDescriptor.begin(), ::tolower);

  map<string, MTGCard*>::iterator cached = mtgCardByNameCache.find(nameDescriptor);

  if (cached != mtgCardByNameCache.end())
  {
    return cached->second;
  }

  int cardnb = atoi(nameDescriptor.c_str());
  if (cardnb)
  {
    MTGCard* result = get_card_from_id(cardnb);
    mtgCardByNameCache[nameDescriptor] = result;
    return result;
  }

  int setId = -1;
  size_t found = nameDescriptor.find(" (");
  string name = nameDescriptor;
  if (found != string::npos)
  {
    size_t end = nameDescriptor.find(")");
    string setName = nameDescriptor.substr(found + 2, end - found - 2);
    trim(setName);
    name = nameDescriptor.substr(0, found);
    trim(name);
    setId = setlist[setName];

    // Reconstruct a clean string "name (set)" for cache consistency
    nameDescriptor = name + " (" + setName + ")";
  }
  map<int, MTGCard*>::iterator it;
  for (it = collection.begin(); it != collection.end(); it++)
  {
    MTGCard* c = it->second;
    if (setId != -1 && setId != c->setId)
      continue;
    string cardName = c->data->name;
    std::transform(cardName.begin(), cardName.end(), cardName.begin(), ::tolower);
    if (cardName.compare(name) == 0)
    {
      mtgCardByNameCache[nameDescriptor] = c;
      return c;
    }
  }
  mtgCardByNameCache[nameDescriptor] = NULL;
  return NULL;
}