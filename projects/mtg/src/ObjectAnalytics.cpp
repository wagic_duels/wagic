#include "PrecompiledHeader.h"

#include "ObjectAnalytics.h"

#include "CardPrimitive.h"
#include "ExtraCost.h"
#include "MTGCard.h"
#include "MTGCardInstance.h"
#include "ManaCost.h"

#include <wsl/log.hxx>

namespace ObjectAnalytics
{
  void DumpStatistics()
  {
#ifdef TRACK_OBJECT_USAGE

    WSL_TRACE() << "-----------------------------------------------------------";
    WSL_TRACE() << "CardPrimitive current count: " << InstanceCounter<CardPrimitive>::GetCurrentObjectCount();
    WSL_TRACE() << "CardPrimitive current byte usage: " << InstanceCounter<CardPrimitive>::GetCurrentByteCount();
    WSL_TRACE() << "CardPrimitive max count: " << InstanceCounter<CardPrimitive>::GetMaximumObjectCount();
    WSL_TRACE() << "CardPrimitive max byte usage: " << InstanceCounter<CardPrimitive>::GetMaximumByteCount();

    WSL_TRACE() << "MTGCard current count: " << InstanceCounter<MTGCard>::GetCurrentObjectCount();
    WSL_TRACE() << "MTGCard current byte usage: " << InstanceCounter<MTGCard>::GetCurrentByteCount();
    WSL_TRACE() << "MTGCard max count: " << InstanceCounter<MTGCard>::GetMaximumObjectCount();
    WSL_TRACE() << "MTGCard max byte usage: " << InstanceCounter<MTGCard>::GetMaximumByteCount();

    WSL_TRACE() << "MTGCardInstance current count: " << InstanceCounter<MTGCardInstance>::GetCurrentObjectCount();
    WSL_TRACE() << "MTGCardInstance current byte usage: " << InstanceCounter<MTGCardInstance>::GetCurrentByteCount();
    WSL_TRACE() << "MTGCardInstance max count: " << InstanceCounter<MTGCardInstance>::GetMaximumObjectCount();
    WSL_TRACE() << "MTGCardInstance max byte usage: " << InstanceCounter<MTGCardInstance>::GetMaximumByteCount();

    WSL_TRACE() << "ManaCost current count: " << InstanceCounter<ManaCost>::GetCurrentObjectCount();
    WSL_TRACE() << "ManaCost current byte usage: " << InstanceCounter<ManaCost>::GetCurrentByteCount();
    WSL_TRACE() << "ManaCost max count: " << InstanceCounter<ManaCost>::GetMaximumObjectCount();
    WSL_TRACE() << "ManaCost max byte usage: " << InstanceCounter<ManaCost>::GetMaximumByteCount();

    WSL_TRACE() << "ExtraCost current count: " << InstanceCounter<ExtraCost>::GetCurrentObjectCount();
    WSL_TRACE() << "ExtraCost current byte usage: " << InstanceCounter<ExtraCost>::GetCurrentByteCount();
    WSL_TRACE() << "ExtraCost max count: " << InstanceCounter<ExtraCost>::GetMaximumObjectCount();
    WSL_TRACE() << "ExtraCost max byte usage: " << InstanceCounter<ExtraCost>::GetMaximumByteCount();
    WSL_TRACE() << "-----------------------------------------------------------";

#endif
  }
} // namespace ObjectAnalytics
