
#include "MTGSets.h"
#include "config.h"

#include "GameOptions.h"
#include "MTGCard.h"
#include "MTGDefinitions.h"
#include "MTGPack.h"
#include "utils.h"

#include <wsl/log.hxx>

// MTGSets
MTGSets setlist; // Our global.

MTGSets::MTGSets() {}

MTGSets::~MTGSets() { clear(); }

void MTGSets::clear()
{
  for (size_t i = 0; i < setinfo.size(); ++i)
  {
    delete (setinfo[i]);
  }
  blocks.clear();
  setinfo.clear();
}

MTGSetInfo* MTGSets::getInfo(int setID)
{
  if (setID < 0 || setID >= (int)setinfo.size())
    return NULL;

  return setinfo[setID];
}

MTGSetInfo* MTGSets::randomSet(int blockId, int atleast)
{
  char* unlocked = (char*)calloc(size(), sizeof(char));

  // Figure out which sets are available.
  for (int i = 0; i < size(); i++)
  {
    unlocked[i] = options[Options::optionSet(i)].number;
  }
  // No luck randomly. Now iterate from a random location.
  int a = 0, iter = 0;
  while (iter < 3)
  {
    a = rand() % size();
    for (int i = a; i < size(); i++)
    {
      if (unlocked[i] && (blockId == -1 || setinfo[i]->block == blockId) &&
          (atleast == -1 || setinfo[i]->totalCards() >= atleast))
      {
        free(unlocked);
        return setinfo[i];
      }
    }
    for (int i = 0; i < a; i++)
    {
      if (unlocked[i] && (blockId == -1 || setinfo[i]->block == blockId) &&
          (atleast == -1 || setinfo[i]->totalCards() >= atleast))
      {
        free(unlocked);
        return setinfo[i];
      }
    }
    blockId = -1;
    iter++;
    if (iter == 2)
      atleast = -1;
  }
  free(unlocked);
  return NULL;
}

int blockSize(int blockId);

int MTGSets::Add(const string& name)
{
  int setid = findSet(name);
  if (setid != -1)
    return setid;

  MTGSetInfo* s = NEW MTGSetInfo(name);
  setinfo.push_back(s);
  setid = (int)setinfo.size();

  return setid - 1;
}

int MTGSets::findSet(string name)
{
  std::transform(name.begin(), name.end(), name.begin(), ::tolower);

  for (int i = 0; i < (int)setinfo.size(); i++)
  {
    MTGSetInfo* s = setinfo[i];
    if (!s)
      continue;
    string set = s->id;
    std::transform(set.begin(), set.end(), set.begin(), ::tolower);
    if (set.compare(name) == 0)
      return i;
  }
  return -1;
}

int MTGSets::findBlock(string s)
{
  if (!s.size())
    return -1;

  string comp = s;
  std::transform(comp.begin(), comp.end(), comp.begin(), ::tolower);
  for (int i = 0; i < (int)blocks.size(); i++)
  {
    string b = blocks[i];
    std::transform(b.begin(), b.end(), b.begin(), ::tolower);
    if (b.compare(comp) == 0)
      return i;
  }

  blocks.push_back(s);
  return ((int)blocks.size()) - 1;
}

int MTGSets::operator[](string id) { return findSet(id); }

string MTGSets::operator[](int id)
{
  if (id < 0 || id >= (int)setinfo.size())
    return "";

  MTGSetInfo* si = setinfo[id];
  if (!si)
    return "";

  return si->id;
}

int MTGSets::getSetNum(MTGSetInfo* i)
{
  int it;
  for (it = 0; it < size(); it++)
  {
    if (setinfo[it] == i)
      return it;
  }
  return -1;
}
int MTGSets::size() { return (int)setinfo.size(); }

// MTGSetInfo
MTGSetInfo::~MTGSetInfo() { SAFE_DELETE(mPack); }

MTGSetInfo::MTGSetInfo(const string& _id)
{
  string whitespaces(" \t\f\v\n\r");
  id = _id;
  block = -1;
  year = -1;
  total = -1;

  for (int i = 0; i < MTGSetInfo::MAX_COUNT; i++)
    counts[i] = 0;

  char myFilename[4096];
  sprintf(myFilename, "sets/%s/booster.txt", id.c_str());
  mPack = NEW MTGPack(myFilename);
  if (!mPack->isValid())
  {
    SAFE_DELETE(mPack);
  }
  bZipped = false;
  bThemeZipped = false;
}

void MTGSetInfo::count(MTGCard* c)
{
  if (!c)
    return;

  switch (c->getRarity())
  {
  case Constants::RARITY_M:
    counts[MTGSetInfo::MYTHIC]++;
    break;
  case Constants::RARITY_R:
    counts[MTGSetInfo::RARE]++;
    break;
  case Constants::RARITY_U:
    counts[MTGSetInfo::UNCOMMON]++;
    break;
  case Constants::RARITY_C:
    counts[MTGSetInfo::COMMON]++;
    break;
  default:
  case Constants::RARITY_L:
    counts[MTGSetInfo::LAND]++;
    break;
  }

  counts[MTGSetInfo::TOTAL_CARDS]++;
}

int MTGSetInfo::totalCards() { return counts[MTGSetInfo::TOTAL_CARDS]; }

string MTGSetInfo::getName()
{
  if (name.size())
    return name; // Pretty name is translated when rendering.
  return id;     // Ugly name as well.
}

string MTGSetInfo::getBlock()
{
  if (block < 0 || block >= (int)setlist.blocks.size())
    return "None";

  return setlist.blocks[block];
}

void MTGSetInfo::processConfLine(string line)
{
  size_t i = line.find_first_of("=");
  if (i == string::npos)
    return;

  string key = line.substr(0, i);
  std::transform(key.begin(), key.end(), key.begin(), ::tolower);
  string value = line.substr(i + 1);

  if (key.compare("name") == 0)
    name = value;
  else if (key.compare("author") == 0)
    author = value;
  else if (key.compare("block") == 0)
    block = setlist.findBlock(value.c_str());
  else if (key.compare("year") == 0)
    year = atoi(value.c_str());
  else if (key.compare("total") == 0)
    total = atoi(value.c_str());
}
