#include "PrecompiledHeader.h"

#include "AbilityParser.h"
#include "DeckManager.h"
#include "DeckMetaData.h"
#include "MTGCollection.h"
#include "MTGDeck.h"
#include "MTGPack.h"
#include "PriceList.h"
#include "Subtypes.h"
#include "Translate.h"
#include "WDataSrc.h"
#include "utils.h"

#include <wsl/log.hxx>

#include <iomanip>

#if defined(WIN32) || defined(LINUX)
#include <time.h>
#endif

// MTGDeck
MTGDeck::MTGDeck(MTGAllCards* _allcards)
{
  total_cards = 0;
  database = _allcards;
  filename = "";
  meta_name = "";
}

int MTGDeck::totalPrice()
{
  int total = 0;
  PriceList* pricelist = NEW PriceList("settings/prices.dat", MTGCollection());
  map<int, int>::iterator it;
  for (it = cards.begin(); it != cards.end(); it++)
  {
    int nb = it->second;
    if (nb)
      total += pricelist->getPrice(it->first);
  }
  SAFE_DELETE(pricelist);
  return total;
}

MTGDeck::MTGDeck(const string& config_file, MTGAllCards* _allcards, int meta_only, int difficultyRating)
{
  total_cards = 0;
  database = _allcards;
  filename = config_file;
  size_t slash = filename.find_last_of("/");
  size_t dot = filename.find(".");
  meta_name = filename.substr(slash + 1, dot - slash - 1);
  meta_id = atoi(meta_name.substr(4).c_str());
  std::string contents;
  if (JFileSystem::GetInstance()->readIntoString(config_file, contents))
  {
    std::stringstream stream(contents);
    std::string s;
    while (std::getline(stream, s))
    {
      if (!s.size())
        continue;
      if (s[s.size() - 1] == '\r')
        s.erase(s.size() - 1); // Handle DOS files
      if (!s.size())
        continue;
      if (s[0] == '#')
      {
        size_t found = s.find("NAME:");
        if (found != string::npos)
        {
          meta_name = s.substr(found + 5);
          continue;
        }
        found = s.find("DESC:");
        if (found != string::npos)
        {
          if (meta_desc.size())
            meta_desc.append("\n");
          meta_desc.append(s.substr(found + 5));
          continue;
        }
        found = s.find("HINT:");
        if (found != string::npos)
        {
          meta_AIHints.push_back(s.substr(found + 5));
          continue;
        }
        found = s.find("UNLOCK:");
        if (found != string::npos)
        {
          meta_unlockRequirements = s.substr(found + 7);
          continue;
        }
        found = s.find("SB:");
        if (found != string::npos)
        {
          Sideboard.push_back(s.substr(found + 3));
          continue;
        }
        continue;
      }
      if (meta_only)
        break;
      int numberOfCopies = 1;
      size_t found = s.find(" *");
      if (found != string::npos)
      {
        numberOfCopies = atoi(s.substr(found + 2).c_str());
        s = s.substr(0, found);
      }
      size_t diff = s.find("toggledifficulty:");
      if (diff != string::npos)
      {
        string cards = s.substr(diff + 17);
        size_t separator = cards.find("|");
        string cardeasy = cards.substr(0, separator);
        string cardhard = cards.substr(separator + 1);
        if (difficultyRating == HARD)
        {
          s = cardhard;
        }
        else
        {
          s = cardeasy;
        }
      }
      MTGCard* card = database->getCardByName(s);
      if (card)
      {
        for (int i = 0; i < numberOfCopies; i++)
        {
          add(card);
        }
      }
      else
      {
        DebugTrace("could not find Card matching name: " << s);
      }
    }
  }
  else
  {
    DebugTrace("FATAL:MTGDeck.cpp:MTGDeck - can't load deck file");
  }
}

int MTGDeck::totalCards() { return total_cards; }

string MTGDeck::getFilename() { return filename; }

MTGCard* MTGDeck::getCardById(int mtgId) { return database->get_card_from_id(mtgId); }

int MTGDeck::addRandomCards(
    int howmany, int* setIds, int nbSets, int rarity, const string& _subtype, int* colors, int nbcolors)
{
  if (howmany <= 0)
    return 1;

  vector<int> unallowedColors;
  unallowedColors.resize(Constants::NB_Colors + 1);
  for (int i = 0; i < Constants::NB_Colors; ++i)
  {
    if (nbcolors)
      unallowedColors[i] = 1;
    else
      unallowedColors[i] = 0;
  }
  for (int i = 0; i < nbcolors; ++i)
  {
    unallowedColors[colors[i]] = 0;
  }

  int collectionTotal = database->totalCards();
  if (!collectionTotal)
    return 0;

  string subtype;
  if (_subtype.size())
    subtype = _subtype;

  vector<int> subcollection;
  int subtotal = 0;
  for (int i = 0; i < collectionTotal; i++)
  {
    MTGCard* card = database->get_card_from_idx(i);
    int r = card->getRarity();
    if (r != Constants::RARITY_T && (rarity == -1 || r == rarity) && // remove tokens
        card->setId != MTGSets::INTERNAL_SET && // remove cards that are defined in primitives. Those are workarounds
                                                // (usually tokens) and should only be used internally
        (!_subtype.size() || card->data->hasSubtype(subtype)))
    {
      int ok = 0;

      if (!nbSets)
        ok = 1;
      for (int j = 0; j < nbSets; ++j)
      {
        if (card->setId == setIds[j])
        {
          ok = 1;
          break;
        }
      }

      if (ok)
      {
        for (int j = 0; j < Constants::NB_Colors; ++j)
        {
          if (unallowedColors[j] && card->data->hasColor(j))
          {
            ok = 0;
            break;
          }
        }
      }

      if (ok)
      {
        subcollection.push_back(card->getId());
        subtotal++;
      }
    }
  }
  if (subtotal == 0)
  {
    if (rarity == Constants::RARITY_M)
      return addRandomCards(howmany, setIds, nbSets, Constants::RARITY_R, _subtype, colors, nbcolors);
    return 0;
  }
  for (int i = 0; i < howmany; i++)
  {
    int id = (rand() % subtotal);
    add(subcollection[id]);
  }
  return 1;
}

int MTGDeck::add(MTGDeck* deck)
{
  map<int, int>::iterator it;
  for (it = deck->cards.begin(); it != deck->cards.end(); it++)
  {
    for (int i = 0; i < it->second; i++)
    {
      add(it->first);
    }
  }
  return deck->totalCards();
}

int MTGDeck::add(int cardid)
{
  if (!database->get_card_from_id(cardid))
    return 0;
  if (cards.find(cardid) == cards.end())
  {
    cards[cardid] = 1;
  }
  else
  {
    cards[cardid]++;
  }
  ++total_cards;
  return total_cards;
}

int MTGDeck::add(MTGCard* card)
{
  if (!card)
    return 0;
  return (add(card->getId()));
}

int MTGDeck::complete()
{
  /* (PSY) adds cards to the deck/collection. Makes sure that the deck
  or collection has at least 4 of every implemented card. Does not
  change the number of cards of which already 4 or more are present. */

  constexpr unsigned short max_number_of_cards = 4;
  const auto databaseSize = database->database_size();
  for (size_t it = 0; it < databaseSize; it++)
  {
    const auto id = database->id_at(it);
    if (!database->get_card_from_id(id)->data->hasType("nothing"))
    {
      if (cards.find(id) == cards.end())
      {
        cards[id] = max_number_of_cards;
        total_cards += max_number_of_cards;
      }
      else
      {
        const auto n = cards[id];
        if (n < max_number_of_cards)
        {
          total_cards += max_number_of_cards - n;
          cards[id] = max_number_of_cards;
        }
      }
    }
  }
  return 1;
}

int MTGDeck::removeAll()
{
  total_cards = 0;
  cards.clear();
  return 1;
}

void MTGDeck::replaceSB(vector<string> newSB)
{
  if (newSB.size())
  {
    Sideboard.clear();
    Sideboard = newSB;
  }
  return;
}

int MTGDeck::remove(int cardid)
{
  if (cards.find(cardid) == cards.end() || cards[cardid] == 0)
    return 0;
  cards[cardid]--;
  total_cards--;
  return 1;
}

int MTGDeck::remove(MTGCard* card)
{
  if (!card)
    return 0;
  return (remove(card->getId()));
}

int MTGDeck::save() { return save(filename, false, meta_name, meta_desc); }

int MTGDeck::save(const string& destFileName,
                  bool useExpandedDescriptions,
                  const string& deckTitle,
                  const string& deckDesc)
{
  string tmp = destFileName;
  tmp.append(".tmp"); // not thread safe
  std::ofstream file;
  if (JFileSystem::GetInstance()->openForWrite(file, tmp))
  {
    char writer[512];
    DebugTrace("Saving Deck: " << deckTitle << " to " << destFileName);
    if (meta_name.size())
    {
      file << "#NAME:" << deckTitle << '\n';
    }

    if (meta_desc.size())
    {
      size_t found = 0;
      string desc = deckDesc;
      found = desc.find_first_of("\n");
      while (found != string::npos)
      {
        file << "#DESC:" << desc.substr(0, found + 1);
        desc = desc.substr(found + 1);
        found = desc.find_first_of("\n");
      }
      file << "#DESC:" << desc << "\n";
    }

    bool saveDetailedDeckInfo = options.get(Options::SAVEDETAILEDDECKINFO)->number == 1;

    if (filename.find("collection.dat") != string::npos)
      saveDetailedDeckInfo = false;

    if (useExpandedDescriptions || saveDetailedDeckInfo)
    {
      printDetailedDeckText(file);
    }
    else
    {
      map<int, int>::iterator it;
      for (it = cards.begin(); it != cards.end(); it++)
      {
        sprintf(writer, "%i\n", it->first);
        for (int j = 0; j < it->second; j++)
        {
          file << writer;
        }
      }
    }
    // save sideboards
    if (Sideboard.size())
    {
      sort(Sideboard.begin(), Sideboard.end());
      for (unsigned int k = 0; k < Sideboard.size(); k++)
      {
        int checkID = atoi(Sideboard[k].c_str());
        if (checkID)
          file << "#SB:" << checkID << "\n";
      }
    }

    file.close();
    JFileSystem::GetInstance()->Rename(tmp, destFileName);
  }
  return 1;
}

/***
    print out an expanded version of the deck to file.
    This save meta data about each card to allow easy reading of the deck file.  It will
    also save each card by id, to speed up the loading of the deck next time.
*/
void MTGDeck::printDetailedDeckText(std::ofstream& file)
{
  ostringstream currentCard, creatures, lands, spells, types;
  ostringstream ss_creatures, ss_lands, ss_spells;
  int numberOfCreatures = 0;
  int numberOfSpells = 0;
  int numberOfLands = 0;

  map<int, int>::iterator it;
  for (it = cards.begin(); it != cards.end(); it++)
  {
    int cardId = it->first;
    int nbCards = it->second;
    MTGCard* card = this->getCardById(cardId);
    if (card == NULL)
    {
      continue;
    }
    MTGSetInfo* setInfo = setlist.getInfo(card->setId);
    string setName = setInfo->id;
    string cardName = card->data->getName();

    currentCard << "#" << nbCards << "x " << cardName << " (" << setName << "), ";

    if (!card->data->isLand())
      currentCard << card->data->getManaCost() << ", ";

    // Add the card's types
    vector<int>::iterator typeIter;
    for (typeIter = card->data->types.begin(); typeIter != card->data->types.end(); ++typeIter)
      types << MTGAllCards::findType(*typeIter) << " ";

    currentCard << trim(types.str()) << ", ";
    types.str(""); // reset the buffer.

    // Add P/T if a creature
    if (card->data->isCreature())
      currentCard << card->data->getPower() << "/" << card->data->getToughness() << ", ";

    if (card->data->getOtherRestrictions().size())
      currentCard << ", " << card->data->getOtherRestrictions();

    for (size_t x = 0; x < card->data->basicAbilities.size(); ++x)
    {
      if (card->data->basicAbilities[x] == 1)
        currentCard << Constants::MTGBasicAbilities[x] << "; ";
    }
    currentCard << endl;

    for (int i = 0; i < nbCards; i++)
      currentCard << cardId << endl;

    currentCard << endl;
    setInfo = NULL;
    // Add counter to know number of creatures, non-creature spells and lands present in the deck
    if (card->data->isLand())
    {
      lands << currentCard.str();
      numberOfLands += nbCards;
    }
    else if (card->data->isCreature())
    {
      creatures << currentCard.str();
      numberOfCreatures += nbCards;
    }
    else
    {
      spells << currentCard.str();
      numberOfSpells += nbCards;
    }
    currentCard.str("");
  }
  ss_creatures << numberOfCreatures;
  ss_spells << numberOfSpells;
  ss_lands << numberOfLands;

  file << getCardBlockText("Creatures x" + ss_creatures.str(), creatures.str());
  file << getCardBlockText("Spells x" + ss_spells.str(), spells.str());
  file << getCardBlockText("Lands x" + ss_lands.str(), lands.str());
  creatures.str("");
  spells.str("");
  lands.str("");
}

/***
 * Convience method to print out blocks of card descriptions
 */
string MTGDeck::getCardBlockText(const string& title, const string& text)
{
  ostringstream oss;
  string textBlock(text);

  oss << setfill('#') << setw(40) << "#" << endl;
  oss << "#    " << setfill(' ') << setw(34) << left << title << "#" << endl;
  oss << setfill('#') << setw(40) << "#" << endl;
  oss << trim(textBlock) << endl;

  return oss.str();
}
