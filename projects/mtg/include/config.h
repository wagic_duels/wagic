#ifndef _DEBUG_H_
#define _DEBUG_H_

#if ((defined WIN32) || (defined WP8)) && !defined(__MINGW32__)
#define snprintf sprintf_s
#endif

#if (defined(WIN32) || defined(LINUX)) && defined(_DEBUG)
#define TESTSUITE 1
#endif

#ifdef _DEBUG
#define TRACK_OBJECT_USAGE
#endif

#include "limits.h"

#if defined(_DEBUG) && defined(WIN32) && (!defined LINUX)
#include "crtdbg.h"
#define NEW new (_NORMAL_BLOCK, __FILE__, __LINE__)
#else
#define NEW new
#endif

#ifndef RESPATH
#define RESPATH "Res"
#endif

#ifndef MAX
#define MAX(a, b) (((a) > (b)) ? (a) : (b))
#endif
#ifndef MIN
#define MIN(a, b) (((a) < (b)) ? (a) : (b))
#endif

#endif
