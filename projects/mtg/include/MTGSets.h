#ifndef WAGIC_MTGSETS_HXX
#define WAGIC_MTGSETS_HXX

#include <string>
#include <vector>

class MTGCard;
class MTGPack;

class MTGSetInfo
{
public:
  MTGSetInfo(const std::string& _id);
  ~MTGSetInfo();
  std::string id;     // Short name: 10E, RAV, etc. Automatic from folder.
  std::string author; // Author of set, for crediting mod makers, etc.
  std::string name;   // Long name: Tenth Edition
  int block;          // For future use by tournament mode, etc.
  int year;           // The year the set was released.
  int total;          // total cards
  // TODO Way to group cards by name, rather than mtgid.

  void count(MTGCard* c);

  int totalCards();
  std::string getName();
  std::string getBlock();
  void processConfLine(std::string line);

  enum
  {
    // For memoized counts
    LAND = 0,
    COMMON = 1,
    UNCOMMON = 2,
    RARE = 3,
    MAX_RARITY = 4, // For boosters, mythic is part of rare... always.
    MYTHIC = 4,
    TOTAL_CARDS = 5,
    MAX_COUNT = 6
  };

  MTGPack* mPack;    // Does it use a specialized booster pack?
  bool bZipped;      // Is this set's images present as a zip file?
  bool bThemeZipped; //[...] in the theme?
  int counts[MTGSetInfo::MAX_COUNT];
};

class MTGSets
{
public:
  // These values have to be < 0
  // A setID with a value >=0 will be looked into the sets table,
  // Negative values will be compared to these enums throughout the code (shop, filters...)
  enum
  {
    INTERNAL_SET = -1,
    ALL_SETS = -2,
  };

  friend class MTGSetInfo;
  MTGSets();
  ~MTGSets();

  int Add(const std::string& subtype);
  int findSet(std::string value);
  int findBlock(std::string s);
  int size();

  int getSetNum(MTGSetInfo* i);

  int operator[](std::string id); // Returns set id index, -1 for failure.
  std::string operator[](int id); // Returns set id name, "" for failure.

  MTGSetInfo* getInfo(int setID);
  MTGSetInfo* randomSet(int blockId = -1, int atleast = -1); // Tries to match, otherwise 100% random unlocked set

  void clear();

protected:
  std::vector<std::string> blocks;
  std::vector<MTGSetInfo*> setinfo;
};
extern MTGSets setlist;

#endif /*WAGIC_MTGSETS_HXX*/