#ifndef WAGIC_MTGCOLLECTION_H
#define WAGIC_MTGCOLLECTION_H

#include <config.h>

#include <MTGSets.h>
#include <Subtypes.h>

#include <wsl/mutex.hxx>

#include <map>
#include <string>
#include <vector>

class MTGCard;
class CardPrimitive;

class MTGAllCards final
{
  using database_type = std::map<int, MTGCard*>;
  using primitives_type = std::map<std::string, CardPrimitive*>;

public:
  using size_type = std::size_t;

  using iterator = typename database_type::iterator;
  using const_iterator = typename database_type::const_iterator;

  MTGAllCards(const MTGAllCards&) = delete;
  MTGAllCards& operator=(const MTGAllCards&) = delete;

  MTGAllCards(MTGAllCards&&) = delete;
  MTGAllCards& operator=(MTGAllCards&&) = delete;

#ifdef TESTSUITE
  void prefetchCardNameCache();
#endif

  //////////////////////////////////////////////////////////////////////////////
  // ID ACCESS
  inline size_type database_size() const noexcept { return ids.size(); }
  inline int id_at(size_type pos) const { return ids.at(pos); }
  //////////////////////////////////////////////////////////////////////////////

  //////////////////////////////////////////////////////////////////////////////
  // COLLECTION ACCESS

  inline int totalCards() const noexcept { return static_cast<int>(database_size()); }
  inline size_type collection_size() const noexcept { return collection.size(); }

  MTGCard* get_card_from_id(int id) const noexcept;
  MTGCard* get_card_from_idx(size_type idx) const noexcept;

  inline const_iterator cbegin() const noexcept { return collection.cbegin(); }
  inline const_iterator cend() const noexcept { return collection.cend(); }
  //////////////////////////////////////////////////////////////////////////////

  //////////////////////////////////////////////////////////////////////////////
  // PRIMITIVES ACCESS
  inline size_type primitives_size() const noexcept { return primitives.size(); }
  //////////////////////////////////////////////////////////////////////////////

  MTGCard* getCardByName(std::string name);
  void loadFolder(const std::string& folder, const std::string& filename = "");

  int load(const std::string& config_file);
  int load(const std::string& config_file, const std::string& setName);
  int load(const std::string& config_file, int set_id);
  int countByType(const std::string& _type);
  int countByColor(int color);
  int countBySet(int setId);
  int randomCardId();

  static int findType(std::string subtype, bool forceAdd = true)
  {
    wsl::scoped_lock<wsl::mutex> lock(instance->mMutex);
    int result = instance->subtypesList.find(subtype, forceAdd);
    return result;
  };
  static int add(std::string value, unsigned int parentType)
  {
    wsl::scoped_lock<wsl::mutex> lock(instance->mMutex);
    int result = instance->subtypesList.add(value, parentType);
    return result;
  };
  static std::string findType(unsigned int id)
  {
    wsl::scoped_lock<wsl::mutex> lock(instance->mMutex);
    return instance->subtypesList.find(id);
  };
  static const std::vector<std::string>& getValuesById()
  {
    wsl::scoped_lock<wsl::mutex> lock(instance->mMutex);
    return instance->subtypesList.getValuesById();
  };
  static const std::vector<std::string>& getCreatureValuesById()
  {
    wsl::scoped_lock<wsl::mutex> lock(instance->mMutex);
    return instance->subtypesList.getCreatureValuesById();
  };
  static bool isSubtypeOfType(unsigned int subtype, unsigned int type)
  {
    wsl::scoped_lock<wsl::mutex> lock(instance->mMutex);
    return instance->subtypesList.isSubtypeOfType(subtype, type);
  };
  static bool isSuperType(unsigned int type)
  {
    wsl::scoped_lock<wsl::mutex> lock(instance->mMutex);
    return instance->subtypesList.isSuperType(type);
  };
  static bool isType(unsigned int type)
  {
    wsl::scoped_lock<wsl::mutex> lock(instance->mMutex);
    return instance->subtypesList.isType(type);
  };
  static bool isSubType(unsigned int type)
  {
    wsl::scoped_lock<wsl::mutex> lock(instance->mMutex);
    return instance->subtypesList.isSubType(type);
  };

  static void sortSubtypeList()
  {
    wsl::scoped_lock<wsl::mutex> lock(instance->mMutex);
    instance->subtypesList.sortSubTypes();
  }

  static int findSubtypeId(std::string value) { return instance->subtypesList.find(value, false); }

  static void unloadAll();
  static MTGAllCards* getInstance();

private:
  MTGAllCards();
  ~MTGAllCards();

  enum
  {
    READ_ANYTHING = 0,
    READ_CARD = 1,
    READ_METADATA = 2,
  };

  database_type collection;
  primitives_type primitives;
  std::vector<int> ids;

  MTGCard* tempCard;            // used by parser
  CardPrimitive* tempPrimitive; // used by parser
  int currentGrade; // used by Parser (we don't want an additional attribute for the primitives for that as it is only
                    // used at load time)
  static MTGAllCards* instance;

  int conf_read_mode;
  std::vector<int> colorsCount;

  wsl::mutex mMutex;
  Subtypes subtypesList;
  std::map<std::string, MTGCard*> mtgCardByNameCache;
  int processConfLine(std::string& s, MTGCard* card, CardPrimitive* primitive);
  bool addCardToCollection(MTGCard* card, int setId);
  CardPrimitive* addPrimitive(CardPrimitive* primitive, MTGCard* card = NULL);
};

#define MTGCollection() MTGAllCards::getInstance()

#endif /*WAGIC_MTGCOLLECTION_H*/