#ifndef WAGIC_UTILITIES_HXX
#define WAGIC_UTILITIES_HXX

#include <wsl/optional.hxx>
#include <wsl/pointer.hxx>

#include <map>

namespace wagic
{
  template <typename K, class V>
  class value_map
  {
  public:
    using key_type = K;
    using value_type = V;

  private:
    using map_type = std::map<key_type, value_type>;
    using optional_type = wsl::optional<value_type&>;

  public:
    inline void erase_entry(const key_type type) noexcept
    {
      auto it = m_map.find(type);
      if (it != m_map.end())
      {
        m_map.erase(it);
      }
    }

    inline void set_entry(const key_type type, value_type&& cost) noexcept { m_map[type] = std::move(cost); }

    inline optional_type get_entry(const key_type type) const noexcept
    {
      auto it = m_map.find(type);
      if (it != m_map.end())
      {
        return it->second;
      }
      return {};
    }

    inline optional_type get_entry(const key_type type) noexcept
    {
      auto it = m_map.find(type);
      if (it != m_map.end())
      {
        return it->second;
      }
      return {};
    }

    inline bool has_entry(const key_type type) const noexcept { return m_map.find(type) != m_map.end(); }
    inline void clear() noexcept { m_map.clear(); }

    void swap(value_map& other) noexcept
    {
      using std::swap;
      swap(m_map, other.m_map);
    }

  private:
    map_type m_map;
  };

  // this map is currently only a work around to reduce the memory footprint of some objects
  // the additional lay of indirection provided with pointer (map node ptr) to ptr is not neccessary at all
  // this structure is only a step bevor the pointer indirection can completly removed.
  // the alternate opt provided by nullptr should be handled in a ptr way e.g. with optionals
  template <typename K, class V>
  class value_ptr_map
  {
  public:
    using key_type = K;
    using value_type = V;
    using pointer_type = wsl::unique_ptr<value_type>;

  private:
    using map_type = std::map<key_type, pointer_type>;

  public:
    value_ptr_map() = default;
    value_ptr_map(const value_ptr_map& other) : m_map() { copy(other); }

    inline value_ptr_map& operator=(const value_ptr_map& other)
    {
      value_ptr_map(other).swap(*this);
      return *this;
    }

    inline void erase_entry(const key_type type) noexcept
    {
      auto it = m_map.find(type);
      if (it != m_map.end())
      {
        m_map.erase(it);
      }
    }

    inline void set_entry(const key_type type, pointer_type cost) noexcept
    {
      if (cost != nullptr)
      {
        m_map[type] = std::move(cost);
      }
    }

    inline value_type* get_entry(const key_type type) const noexcept
    {
      auto it = m_map.find(type);
      if (it != m_map.end())
      {
        return it->second.get();
      }
      return nullptr;
    }

    inline void swap(value_ptr_map& other) noexcept
    {
      using std::swap;
      swap(m_map, other.m_map);
    }

    inline bool has_entry(const key_type type) const noexcept { return get_entry(type) != nullptr; }
    inline void clear() noexcept { m_map.clear(); }

  private:
    inline void copy(const value_ptr_map& other) noexcept
    {
      m_map.clear();
      for (auto& entry : other.m_map)
      {
        if (entry.second != nullptr)
        {
          m_map[entry.first] = wsl::make_unique<value_type>(*entry.second);
        }
      }
    }

    map_type m_map;
  };

} // namespace wagic

#endif /*WAGIC_UTILITIES_HXX*/