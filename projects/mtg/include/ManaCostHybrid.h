#ifndef _MANACOST_HYBRID_H_
#define _MANACOST_HYBRID_H_

#include <wsl/types.hxx>

#include <ostream>
#include <string>

class ManaCostHybrid
{

public:
  wsl::uint8_t color1;
  wsl::uint8_t color2;
  wsl::uint8_t value1;
  wsl::uint8_t value2;
  ManaCostHybrid();
  ManaCostHybrid(const ManaCostHybrid& hybridManaCost);
  ManaCostHybrid(const ManaCostHybrid* hybridManaCost);
  ManaCostHybrid(int c1, int v1, int c2, int v2);

  void init(int c1, int v1, int c2, int v2);
  int hasColor(int color);
  std::string toString() const;
  int getConvertedCost();
  int getManaSymbols(int color);
  int getManaSymbolsHybridMerged(int color);
  void reduceValue(int color, int value);

  friend std::ostream& operator<<(std::ostream& out, const ManaCostHybrid& m);
  friend std::ostream& operator<<(std::ostream& out, ManaCostHybrid* m);
};

#endif
