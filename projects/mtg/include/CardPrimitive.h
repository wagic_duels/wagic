/* CardPrimitive objects represent the cards database.
 * For MTG we have thousands of those, that stay constantly in Ram
 * on low-end devices such as the PSP, adding stuff to this class can have a very high cost
 * As an example, with 16'000 card primitives (the rough number of cards in MTG), adding a simple 16 bytes attribute
 * costs 250kB (2% of the total available ram on the PSP)
 */
#ifndef _CARDPRIMITIVE_H_
#define _CARDPRIMITIVE_H_

#include "config.h"

#include "ManaCost.h"
#include "ObjectAnalytics.h"

#include <bitset>
#include <map>
#include <string>
#include <vector>

#include <wsl/pointer.hxx>
#include <wsl/types.hxx>

using namespace std;

constexpr uint8_t kColorBitMask_Artifact = 0x01;
constexpr uint8_t kColorBitMask_Green = 0x02;
constexpr uint8_t kColorBitMask_Blue = 0x04;
constexpr uint8_t kColorBitMask_Red = 0x08;
constexpr uint8_t kColorBitMask_Black = 0x10;
constexpr uint8_t kColorBitMask_White = 0x20;
constexpr uint8_t kColorBitMask_Land = 0x40;

class CastRestrictions
{
public:
  string restriction;
  string otherrestriction;

  CastRestrictions* clone() const { return NEW CastRestrictions(*this); };
};

class CardPrimitive
#ifdef TRACK_OBJECT_USAGE
    : public InstanceCounter<CardPrimitive>
#endif
{
private:
  enum class ability_types : wsl::uint8_t
  {
    double_faced,
    crew,
    phase_out,
    modular,
    ai_custom_code
  };

  using ability_map = std::map<ability_types, std::string>;

  CastRestrictions* restrictions;
  wsl::int8_t m_dredge_amount;
  wsl::int8_t m_suspended_time;

  ability_map m_ability_map;

  void erase_ability(ability_types ability) noexcept;
  std::string get_ability(ability_types ability) const noexcept;
  void set_ability(ability_types ability, const std::string& str) noexcept;

  std::string m_text;
  std::vector<std::string> m_formatted_text;

protected:
  ManaCost manaCost;

public:
  string name;
  int init();

  wsl::uint8_t colors;
  typedef std::bitset<Constants::NB_BASIC_ABILITIES> BasicAbilitiesSet;
  BasicAbilitiesSet basicAbilities;
  BasicAbilitiesSet LKIbasicAbilities;

  map<string, string> magicTexts;
  string magicText;
  int alias;
  string spellTargetType;
  int power;
  int toughness;

  const std::string& get_text() const noexcept { return m_text; }
  void set_text(const std::string& value) noexcept { m_text = value; }
  const std::vector<string>& get_formatted_text(bool noremove = false);
  inline void set_formatted_text(const std::vector<string>& formatted) noexcept { m_formatted_text = formatted; }

  inline void setdoubleFaced(const string& value) noexcept { set_ability(ability_types::double_faced, value); }
  inline string getdoubleFaced() const noexcept { return get_ability(ability_types::double_faced); }
  inline void setAICustomCode(const string& value) noexcept { set_ability(ability_types::ai_custom_code, value); }
  inline string getAICustomCode() const noexcept { return get_ability(ability_types::ai_custom_code); }
  inline void setCrewAbility(const string& value) noexcept { set_ability(ability_types::crew, value); }
  inline string getCrewAbility() const noexcept { return get_ability(ability_types::crew); }
  inline void setPhasedOutAbility(const string& value) noexcept { set_ability(ability_types::phase_out, value); }
  inline string getPhasedOutAbility() const noexcept { return get_ability(ability_types::phase_out); }
  inline void setModularValue(const string& value) noexcept { set_ability(ability_types::modular, value); }
  inline string getModularValue() const noexcept { return get_ability(ability_types::modular); }

  inline wsl::int8_t get_dredge_amount() const noexcept { return m_dredge_amount; }
  inline void set_dredge_amount(const wsl::int8_t amount) noexcept { m_dredge_amount = amount; }

  inline wsl::int8_t get_suspended_time() const noexcept { return m_suspended_time; }
  inline void set_suspended_time(const wsl::int8_t time) noexcept { m_suspended_time = time; }

  vector<int> types;
  CardPrimitive();
  CardPrimitive(wsl::not_null<CardPrimitive*> source);
  virtual ~CardPrimitive();

  void setColor(int _color, int removeAllOthers = 0);
  void setColor(const string& _color, int removeAllOthers = 0);
  void removeColor(int color);
  int getColor();
  bool hasColor(int inColor);
  int countColors();

  static uint8_t ConvertColorToBitMask(int inColor);

  int has(int ability);

  void addMagicText(string value);
  void addMagicText(string value, string zone);

  inline void setName(const string& value) noexcept { name = value; }
  inline const string& getName() const noexcept { return name; }
  string getLCName() const;

  void addType(char* type_text);
  void addType(int id);
  void setType(const string& type_text);
  void setSubtype(const string& value);
  int removeType(string value, int removeAll = 0);
  int removeType(int value, int removeAll = 0);
  bool hasSubtype(int _subtype);
  bool hasSubtype(const string& _subtype);
  bool hasType(int _type);
  bool hasType(const string& type);

  void setManaCost(const string& value);
  ManaCost* getManaCost();
  bool isCreature();
  bool isLand();
  bool isSpell();
  bool isPermanent();
  bool isSorceryorInstant();
  void setPower(int _power);
  int getPower();
  void setToughness(int _toughness);
  int getToughness();
  void setRestrictions(string _restriction);
  const string getRestrictions();
  void setOtherRestrictions(string _restriction);
  const string getOtherRestrictions();
};

#endif
