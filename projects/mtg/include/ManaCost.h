#ifndef _MANACOST_H_
#define _MANACOST_H_

#include "config.h"

#include "MTGDefinitions.h"
#include "ObjectAnalytics.h"
#include "utils.h"

#include "utilities/value_map.hxx"

#include <wsl/pointer.hxx>

class ManaCostHybrid;
class ExtraCosts;
class ExtraCost;
class MTGAbility;
class MTGCardInstance;
class Player;

class ManaCost
#ifdef TRACK_OBJECT_USAGE
    : public InstanceCounter<ManaCost>
#endif
{
  friend std::ostream& operator<<(std::ostream& out, const ManaCost& m);
  friend std::ostream& operator<<(std::ostream& out, const wsl::not_null<ManaCost*>& m);

  std::vector<ManaCostHybrid> hybrids;

  enum class alternate_type : wsl::uint8_t
  {
    alternative,
    suspend,
    kicker,
    buy_back,
    flashback,
    retrace,
    morph,
    bestow,
    used_to_cast
  };

  using manacost_map_type = wagic::value_map<alternate_type, ManaCost>;
  manacost_map_type m_alternate_map;

  void clear();
  inline void erase_alternate(const alternate_type type) noexcept { m_alternate_map.erase_entry(type); }
  inline void set_alternate(const alternate_type type, ManaCost&& cost) noexcept
  {
    m_alternate_map.set_entry(type, std::forward<ManaCost>(cost));
  }

  inline void set_alternate(const alternate_type type, ManaCost* ptr) noexcept
  {
    if (ptr == nullptr)
    {
      m_alternate_map.erase_entry(type);
    }
    else
    {
      set_alternate(type, ManaCost(*ptr));
    }

    SAFE_DELETE(ptr);
  }

  inline ManaCost* get_alternate(const alternate_type type) noexcept
  {
    auto optional_cost = m_alternate_map.get_entry(type);
    if (optional_cost)
    {
      return &(optional_cost.value());
    }
    return nullptr;
  }

protected:
  std::vector<int16_t> cost;

  virtual void init();

public:
  enum
  {
    MANA_UNPAID = 0,
    MANA_PAID = 1,
    MANA_PAID_WITH_KICKER = 2,
    MANA_PAID_WITH_ALTERNATIVE = 3,
    MANA_PAID_WITH_BUYBACK = 4,
    MANA_PAID_WITH_FLASHBACK = 5,
    MANA_PAID_WITH_RETRACE = 6,
    MANA_PAID_WITH_MORPH = 7,
    MANA_PAID_WITH_SUSPEND = 8,
    MANA_PAID_WITH_OVERLOAD = 9,
    MANA_PAID_WITH_BESTOW = 10,
    MANA_PAID_WITH_OTHERCOST = 11
  };
  ExtraCosts* extraCosts = nullptr;

  ManaCost(std::vector<int16_t>& _cost, int nb_elems = 1);
  ManaCost();
  virtual ~ManaCost();

  ManaCost(ManaCost* _manaCost);
  ManaCost(const ManaCost& manaCost);
  ManaCost& operator=(const ManaCost& manaCost);
  void copy(const wsl::not_null<ManaCost*>& _manaCost);

  void swap(ManaCost& other) noexcept;

  inline ManaCost* getSuspend() noexcept { return get_alternate(alternate_type::suspend); };
  inline void setSuspend(ManaCost* aMana) noexcept { set_alternate(alternate_type::suspend, aMana); };

  inline ManaCost* getKicker() noexcept { return get_alternate(alternate_type::kicker); };
  inline void setKicker(ManaCost* aMana) noexcept { set_alternate(alternate_type::kicker, aMana); };

  inline ManaCost* getAlternative() noexcept { return get_alternate(alternate_type::alternative); };
  inline void setAlternative(ManaCost* aMana) noexcept { set_alternate(alternate_type::alternative, aMana); };

  inline ManaCost* getBuyback() noexcept { return get_alternate(alternate_type::buy_back); };
  inline void setBuyback(ManaCost* aMana) noexcept { set_alternate(alternate_type::buy_back, aMana); };

  inline ManaCost* getFlashback() noexcept { return get_alternate(alternate_type::flashback); };
  inline void setFlashback(ManaCost* aMana) noexcept { set_alternate(alternate_type::flashback, aMana); };

  inline ManaCost* getRetrace() noexcept { return get_alternate(alternate_type::retrace); };
  inline void setRetrace(ManaCost* aMana) noexcept { set_alternate(alternate_type::retrace, aMana); };

  inline ManaCost* getMorph() noexcept { return get_alternate(alternate_type::morph); };
  inline void setMorph(ManaCost* aMana) noexcept { set_alternate(alternate_type::morph, aMana); };

  inline ManaCost* getBestow() noexcept { return get_alternate(alternate_type::bestow); };
  inline void setBestow(ManaCost* aMana) noexcept { set_alternate(alternate_type::bestow, aMana); };

  inline ManaCost* getManaUsedToCast() noexcept { return get_alternate(alternate_type::used_to_cast); };
  inline void setManaUsedToCast(ManaCost* aMana) noexcept { set_alternate(alternate_type::used_to_cast, aMana); };

  string alternativeName;
  bool isMulti;
  static ManaCost* parseManaCost(string value, ManaCost* _manacost = NULL, MTGCardInstance* c = NULL);
  static int parseManaSymbol(char symbol);

  inline virtual void resetCosts() { clear(); }
  void x();
  int hasX();
  void specificX(int color = 0);
  int hasSpecificX();
  int xColor;
  int hasAnotherCost();
  void changeCostTo(ManaCost* _manaCost);
  int isNull();
  int getConvertedCost();
  string toString() const;
  int getCost(int color) const;
  int getManaSymbols(int color);
  int getManaSymbolsHybridMerged(int color);
  int countHybridsNoPhyrexian();
  void removeHybrid(ManaCost* _cost);

  // Returns NULL if i is greater than nbhybrids
  ManaCostHybrid* getHybridCost(unsigned int i);
  int hasColor(int color);
  int remove(int color, int value);
  int add(int color, int value);

  //
  // Extra Costs (sacrifice,counters...)
  //
  int addExtraCost(ExtraCost* _cost);
  int addExtraCosts(ExtraCosts* _cost);
  int setExtraCostsAction(MTGAbility* action, MTGCardInstance* card);
  int isExtraPaymentSet();
  int canPayExtra();
  int doPayExtra();
  ExtraCost* getExtraCost(unsigned int i);

  int addHybrid(int c1, int v1, int c2, int v2);
  int tryToPayHybrids(const std::vector<ManaCostHybrid>& _hybrids, int _nbhybrids, std::vector<int16_t>& diff);
  void randomDiffHybrids(ManaCost* _cost, std::vector<int16_t>& diff);
  int add(ManaCost* _cost);
  int remove(ManaCost* _cost);
  int removeAll(int color);
  int pay(ManaCost* _cost);

  // return 1 if _cost can be paid with current data, 0 otherwise
  int canAfford(ManaCost* _cost);

  int isPositive();
  ManaCost* Diff(ManaCost* _cost);
};

class ManaPool final : public ManaCost
{
private:
  Player* player;

public:
  void Empty();
  ManaPool(Player* player);
  ManaPool(ManaCost* _manaCost, Player* player);
  int remove(int color, int value);
  int add(int color, int value, MTGCardInstance* source = NULL, bool extra = false);
  int add(ManaCost* _cost, MTGCardInstance* source = NULL);
  int pay(ManaCost* _cost);
};

#endif
