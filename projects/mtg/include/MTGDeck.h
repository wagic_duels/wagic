#ifndef _MTGDECK_H_
#define _MTGDECK_H_

#define MTG_ERROR -1

#include "MTGDefinitions.h"
#include "WResourceManager.h"
#include <Subtypes.h>

#include <dirent.h>
#include <string>

using std::string;
class CardPrimitive;
class MTGAllCards;

class MTGDeck
{
private:
  string getCardBlockText(const string& title, const string& textBlock);
  void printDetailedDeckText(std::ofstream& file);

protected:
  string filename;
  int total_cards;

public:
  MTGAllCards* database;
  map<int, int> cards;
  string meta_desc;
  string meta_name;
  vector<string> meta_AIHints;
  vector<string> Sideboard;
  string meta_unlockRequirements;

  int meta_id;
  int totalCards();
  int totalPrice();
  MTGDeck(MTGAllCards* _allcards);
  MTGDeck(const string& config_file, MTGAllCards* _allcards, int meta_only = 0, int difficultySetting = 0);
  int addRandomCards(int howmany,
                     int* setIds = NULL,
                     int nbSets = 0,
                     int rarity = -1,
                     const string& subtype = "",
                     int* colors = NULL,
                     int nbcolors = 0);
  int add(int cardid);
  int add(MTGDeck* deck); // adds the contents of "deck" into myself
  int complete();
  int remove(int cardid);
  int removeAll();
  int add(MTGCard* card);
  int remove(MTGCard* card);
  void replaceSB(vector<string> newSB = vector<string>());
  string getFilename();
  int save();
  int save(const string& destFileName, bool useExpandedDescriptions, const string& deckTitle, const string& deckDesc);
  MTGCard* getCardById(int id);
};

#endif
