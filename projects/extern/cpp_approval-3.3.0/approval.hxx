#ifndef WAGIC_TESTS_EXTERN_APPROVAL_HXX
#define WAGIC_TESTS_EXTERN_APPROVAL_HXX

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wall"
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wnon-virtual-dtor"

#include "ApprovalTests.hpp"

#pragma GCC diagnostic pop
#pragma GCC diagnostic pop

#endif /*WAGIC_TESTS_EXTERN_APPROVAL_HXX*/