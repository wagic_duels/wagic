#ifndef OUTPUTCAPTURER_H
#define OUTPUTCAPTURER_H

#include <sstream>
#include <string>

#include <wsl/mutex.hxx>

class OutputCapturer
{
private:
  static std::ostringstream stream;
  static wsl::mutex mMutex;

public:
  static void add(const std::string& s);
  static void debugAndClear();
  static void clear();
};

#endif // OUTPUTCAPTURER_H
