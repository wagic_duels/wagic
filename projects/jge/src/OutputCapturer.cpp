#include "OutputCapturer.h"

#include <wsl/log.hxx>

std::ostringstream OutputCapturer::stream;
wsl::mutex OutputCapturer::mMutex;

void OutputCapturer::add(const std::string& s)
{
  wsl::scoped_lock<wsl::mutex> lock(mMutex);
  stream << s << "\n";
}

void OutputCapturer::debugAndClear()
{
  stream.flush();
  WSL_PRINTF("%s", stream.str());
  clear();
}

void OutputCapturer::clear() { stream.clear(); }
