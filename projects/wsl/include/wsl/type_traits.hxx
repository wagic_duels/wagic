#ifndef WSL_TYPETRAITS_HXX
#define WSL_TYPETRAITS_HXX

#include <type_traits>

namespace wsl
{
  template <bool B, class T = void>
  using enable_if_t = typename std::enable_if<B, T>::type;

  template <class T>
  using remove_cv_t = typename std::remove_cv<T>::type;

  template <class T>
  using remove_const_t = typename std::remove_const<T>::type;

  template <class T>
  using remove_volatile_t = typename std::remove_volatile<T>::type;

  template <class T>
  using remove_reference_t = typename std::remove_reference<T>::type;
} // namespace wsl

#endif // WSL_TYPETRAITS_HXX