#ifndef WSL_THREAD_PSP_THREAD_HXX
#define WSL_THREAD_PSP_THREAD_HXX

#include <wsl/pointer.hxx>
#include <wsl/time.hxx>

#include <functional>
#include <ostream>

#include <pthread.h>

namespace wsl
{
  class thread
  {
  public:
    using native_handle_type = pthread_t;

    struct impl_base;
    typedef shared_ptr<impl_base> shared_base_type;

    class id
    {
      native_handle_type m_thread;

    public:
      id() noexcept : m_thread() {}
      explicit id(native_handle_type thread_id) : m_thread(thread_id) {}

    private:
      friend class thread;

      inline friend bool operator==(thread::id lhs, thread::id rhs) noexcept
      {
        return pthread_equal(lhs.m_thread, rhs.m_thread);
      }
      inline friend bool operator<(thread::id lhs, thread::id rhs) noexcept { return lhs.m_thread < rhs.m_thread; }

      template <class CharT, class Traits>
      friend std::basic_ostream<CharT, Traits>& operator<<(std::basic_ostream<CharT, Traits>& outh,
                                                           thread::id thread_id);
    };

    struct impl_base
    {
      shared_base_type m_this_ptr;

      inline virtual ~impl_base() = default;
      virtual void run() = 0;
    };

    template <typename Function>
    struct impl : public impl_base
    {
      Function function_obj;
      impl(Function&& func) : function_obj(std::forward<Function>(func)) {}
      void run() { function_obj(); }
    };

  private:
    id m_thread_id;

  public:
    thread() noexcept = default;

    thread(const thread&) = delete;
    thread& operator=(const thread&) = delete;

    thread(thread&& other) noexcept { swap(other); }

    template <typename Function, typename... Args>
    explicit thread(Function&& func, Args&&... args)
    {
      start_thread(make_routine(std::bind(std::forward<Function>(func), std::forward<Args>(args)...)));
    }

    ~thread() noexcept
    {
      if (joinable())
        std::terminate();
    }

    thread& operator=(thread&& other) noexcept
    {
      if (joinable())
        std::terminate();
      swap(other);
      return *this;
    }

    inline void swap(thread& other) noexcept { std::swap(m_thread_id, other.m_thread_id); }

    inline bool joinable() const noexcept { return !(m_thread_id == id()); }
    void join();
    void detach();

    thread::id get_id() const noexcept { return m_thread_id; }
    native_handle_type native_handle() { return m_thread_id.m_thread; }

    static unsigned hardware_concurrency() noexcept;

  private:
    void start_thread(shared_base_type base_ptr);

    template <typename function>
    shared_ptr<impl<function>> make_routine(function&& func)
    {
      return std::make_shared<impl<function>>(std::forward<function>(func));
    }
  };

  inline void swap(thread& lhs, thread& rhs) noexcept { lhs.swap(rhs); }

  inline bool operator!=(thread::id lhs, thread::id rhs) noexcept { return !(lhs == rhs); }
  inline bool operator<=(thread::id lhs, thread::id rhs) noexcept { return !(rhs < lhs); }
  inline bool operator>(thread::id lhs, thread::id rhs) noexcept { return rhs < lhs; }
  inline bool operator>=(thread::id lhs, thread::id rhs) noexcept { return !(lhs < rhs); }

  template <class CharT, class Traits>
  inline std::basic_ostream<CharT, Traits>& operator<<(std::basic_ostream<CharT, Traits>& out, thread::id thread_id)
  {
    if (thread_id == thread::id())
      return out << "thread::id of a non-executing thread";
    else
      return out << thread_id.m_thread;
  }

  namespace this_thread
  {

    /// get_id
    inline thread::id get_id() noexcept { return thread::id(pthread_self()); }

    void sleep_for(time::seconds, time::nanoseconds);

    /// sleep_for
    template <typename _Rep, typename _Period>
    inline void sleep_for(const time::duration<_Rep, _Period>& duration_time)
    {
      if (duration_time <= duration_time.zero())
        return;
      time::seconds sec = time::duration_cast<time::seconds>(duration_time);
      time::nanoseconds nsec = time::duration_cast<time::nanoseconds>(duration_time - sec);
      sleep_for(sec, nsec);
    }

    /// sleep_until
    template <typename _Clock, typename _Duration>
    inline void sleep_until(const time::time_point<_Clock, _Duration>& __atime)
    {
      auto __now = _Clock::now();
      if (__now < __atime)
        sleep_for(__atime - __now);
    }

  } // namespace this_thread

  static inline unsigned hardware_concurrency() noexcept { return 2u; }
} // namespace wsl

#endif // WSL_THREAD_PSP_THREAD_HXX