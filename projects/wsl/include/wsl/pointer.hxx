#ifndef WSL_POINTER_HXX
#define WSL_POINTER_HXX

#include "assert.hxx"
#include "type_traits.hxx"
#include "types.hxx"

#include <memory>

namespace wsl
{

  template <class T, class = wsl::enable_if_t<std::is_pointer<T>::value>>
  using owner = T;

  template <class T>
  class not_null
  {
    using value_type = T;

  public:
    static_assert(std::is_assignable<T&, std::nullptr_t>::value, "T cannot be assigned nullptr.");

    template <typename U, typename = wsl::enable_if_t<std::is_convertible<U, T>::value>>
    not_null(U&& u) : m_ptr(std::forward<U>(u))
    {
      Expects(m_ptr != nullptr);
    }

    ~not_null() = default;
    constexpr not_null(not_null&& other) = default;
    constexpr not_null(const not_null& other) = default;
    not_null& operator=(not_null&& other) = default;
    not_null& operator=(const not_null& other) = default;

    template <typename = wsl::enable_if_t<!std::is_same<std::nullptr_t, T>::value>>
    not_null(T u) : m_ptr(u)
    {
      Expects(m_ptr != nullptr);
    }

    template <typename U, typename = wsl::enable_if_t<std::is_convertible<U, T>::value>>
    constexpr not_null(const not_null<U>& other) : not_null(other.get())
    {
    }

    value_type get() const
    {
      Ensures(m_ptr != nullptr);
      return m_ptr;
    }

    constexpr operator value_type() const { return get(); }
    constexpr value_type operator->() const { return get(); }

    not_null(std::nullptr_t) = delete;
    not_null& operator=(std::nullptr_t) = delete;

    not_null(int) = delete;
    not_null& operator=(int) = delete;

    not_null& operator++() = delete;
    not_null& operator--() = delete;
    not_null operator++(int) = delete;
    not_null operator--(int) = delete;
    not_null& operator+(size_t) = delete;
    not_null& operator+=(size_t) = delete;
    not_null& operator-(size_t) = delete;
    not_null& operator-=(size_t) = delete;
    not_null& operator+=(std::ptrdiff_t) = delete;
    not_null& operator-=(std::ptrdiff_t) = delete;
    void operator[](std::ptrdiff_t) const = delete;

  private:
    T m_ptr;
  };

  template <class T>
  not_null<T> make_not_null(T&& t)
  {
    return not_null<wsl::remove_cv_t<wsl::remove_reference_t<T>>>{std::forward<T>(t)};
  }

  template <class T>
  std::ostream& operator<<(std::ostream& os, const not_null<T>& val)
  {
    os << val.get();
    return os;
  }

  template <class T, class U>
  std::ptrdiff_t operator-(const not_null<T>&, const not_null<U>&) = delete;

  template <class T>
  not_null<T> operator-(const not_null<T>&, std::ptrdiff_t) = delete;

  template <class T>
  not_null<T> operator+(const not_null<T>&, std::ptrdiff_t) = delete;

  template <class T>
  not_null<T> operator+(std::ptrdiff_t, const not_null<T>&) = delete;

  template <class T, class U>
  inline constexpr bool operator==(const not_null<T>& lhs, const not_null<U>& rhs) noexcept
  {
    return lhs.get() == rhs.get();
  }

  template <class T, class U>
  inline constexpr bool operator<(const not_null<T>& lhs, const not_null<U>& rhs) noexcept
  {
    return lhs.get() < rhs.get();
  }

  template <class T, class U>
  inline constexpr bool operator!=(const not_null<T>& lhs, const not_null<U>& rhs)
  {
    return !(lhs == rhs);
  }

  template <class T, class U>
  inline constexpr bool operator<=(const not_null<T>& lhs, const not_null<U>& rhs)
  {
    return !(rhs < lhs);
  }

  template <class T, class U>
  inline constexpr bool operator>(const not_null<T>& lhs, const not_null<U>& rhs)
  {
    return (rhs < lhs);
  }

  template <class T, class U>
  inline constexpr bool operator>=(const not_null<T>& lhs, const not_null<U>& rhs)
  {
    return !(lhs < rhs);
  }

} // namespace wsl

namespace wsl
{
  namespace detail
  {

    using std::shared_ptr;
    using std::unique_ptr;

    using std::make_shared;
#if __cplusplus > 201103L
    using std::make_unique;
#else
    namespace internal
    {
      template <typename T>
      struct make_unique
      {
        using single_object = unique_ptr<T>;
      };

      template <typename T>
      struct make_unique<T[]>
      {
        using array = unique_ptr<T[]>;
      };

      template <typename T, size_t Bound>
      struct make_unique<T[Bound]>
      {
        struct invalid_type
        {
        };
      };

      template <typename T>
      using remove_extent_t = typename std::remove_extent<T>::type;

      template <typename T>
      using remove_all_extents_t = typename std::remove_all_extents<T>::type;

    } // namespace internal

    template <typename T, typename... Args>
    inline typename internal::make_unique<T>::single_object make_unique(Args&&... args)
    {
      return unique_ptr<T>(new T(std::forward<Args>(args)...));
    }

    template <typename T>
    inline typename internal::make_unique<T>::array make_unique(std::size_t num)
    {
      return unique_ptr<T>(new internal::remove_extent_t<T>[num]());
    }

    template <typename T, typename... Args>
    inline typename internal::make_unique<T>::invalid_type make_unique(Args&&...) = delete;
#endif

  } // namespace detail
} // namespace wsl

namespace wsl
{

  template <class T>
  class scoped_ptr
  {
  public:
    scoped_ptr(scoped_ptr const&) = delete;
    scoped_ptr& operator=(scoped_ptr const&) = delete;

    scoped_ptr(scoped_ptr&&) = delete;
    scoped_ptr& operator=(scoped_ptr&&) = delete;

    using element_type = T;

    explicit scoped_ptr(element_type* p = 0) noexcept : m_ptr(p) {}

    ~scoped_ptr() noexcept
    {
      if (m_ptr != nullptr)
      {
        delete m_ptr;
      }
    }

    inline void reset(element_type* p = 0) noexcept { this_type(p).swap(*this); }

    inline element_type& operator*() const noexcept { return *m_ptr; }

    inline element_type* operator->() const noexcept { return m_ptr; }

    inline element_type* get() const noexcept { return m_ptr; }

    inline bool is_valid() const noexcept { return m_ptr != nullptr; }
    explicit inline operator bool() const noexcept { return is_valid(); }

    inline void swap(scoped_ptr& b) noexcept
    {
      T* tmp = b.m_ptr;
      b.m_ptr = m_ptr;
      m_ptr = tmp;
    }

  private:
    element_type* m_ptr;
    using this_type = scoped_ptr<T>;
  };

  template <class T>
  inline bool operator==(scoped_ptr<T> const& p, wsl::nullptr_t) noexcept
  {
    return p.get() == nullptr;
  }

  template <class T>
  inline bool operator==(wsl::nullptr_t, scoped_ptr<T> const& p) noexcept
  {
    return p.get() == nullptr;
  }

  template <class T>
  inline bool operator!=(scoped_ptr<T> const& p, wsl::nullptr_t) noexcept
  {
    return p.get() != nullptr;
  }

  template <class T>
  inline bool operator!=(wsl::nullptr_t, scoped_ptr<T> const& p) noexcept
  {
    return p.get() != nullptr;
  }

  template <class T>
  inline void swap(scoped_ptr<T>& a, scoped_ptr<T>& b) noexcept
  {
    a.swap(b);
  }
} // namespace wsl

namespace wsl
{
  using detail::shared_ptr;
  using detail::unique_ptr;

  using detail::make_shared;
  using detail::make_unique;
} // namespace wsl

#endif /*WSL_POINTER_HXX*/