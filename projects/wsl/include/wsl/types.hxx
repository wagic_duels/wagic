#ifndef WSL_TYPES_HXX
#define WSL_TYPES_HXX

#include <cstdint>

namespace wsl
{
  using int8_t = std::int8_t;
  using uint8_t = std::uint8_t;

  using int16_t = std::int16_t;
  using uint16_t = std::uint16_t;

  using int32_t = std::int32_t;
  using uint32_t = std::uint32_t;

  using int64_t = std::int64_t;
  using uint64_t = std::uint64_t;

  using size_t = std::uint64_t;

  using intmax_t = std::intmax_t;
  using uintmax_t = std::uintmax_t;

  using byte_t = std::uint8_t;
  using pixel_t = std::uint32_t;

  using nullptr_t = std::nullptr_t;
} // namespace wsl

#endif // WSL_TYPES_HXX