#ifndef WSL_OPTIONAL_HXX
#define WSL_OPTIONAL_HXX

#include <tl/optional.hpp>

namespace wsl
{

  using tl::optional;
}

#endif /*WSL_OPTIONAL_HXX*/