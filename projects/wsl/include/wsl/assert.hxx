#ifndef WSL_ASSERT_HXX
#define WSL_ASSERT_HXX

#include <exception>
#include <iostream>

#define WSL_STRINGIFY(x) WSL_STRINGIFY_(x)
#define WSL_STRINGIFY_(x) #x

namespace wsl
{
  namespace detail
  {
    inline void fail_fast_assert(bool cond, const char* const message) noexcept
    {
      if (!cond)
      {
        std::cerr << message << std::endl;
        std::terminate();
      }
    }
  } // namespace detail

} // namespace wsl

#define Expects(x)                                                                                                     \
  ::wsl::detail::fail_fast_assert((x), "Precondition violation at " __FILE__ ":" WSL_STRINGIFY(__LINE__));

#define Ensures(x)                                                                                                     \
  ::wsl::detail::fail_fast_assert((x), "Postcondition violation at " __FILE__ ":" WSL_STRINGIFY(__LINE__));

#endif /*WSL_ASSERT_HXX*/