#ifndef WSL_DEBUG_HXX
#define WSL_DEBUG_HXX

namespace wsl
{
  int get_complete_allocated_memory() noexcept;
}

#endif /*WSL_DEBUG_HXX*/