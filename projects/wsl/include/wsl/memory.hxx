#ifndef WSL_MEMORY_HXX
#define WSL_MEMORY_HXX

#include "types.hxx"

namespace wsl
{
  namespace memory
  {
    namespace literals
    {

      constexpr wsl::size_t operator"" _KiB(unsigned long long value) noexcept { return wsl::size_t(value * 1024); }
      constexpr wsl::size_t operator"" _KB(unsigned long long value) noexcept { return wsl::size_t(value * 1000); }

      constexpr wsl::size_t operator"" _MiB(unsigned long long value) noexcept
      {
        return wsl::size_t(value * 1024 * 1024);
      }
      constexpr wsl::size_t operator"" _MB(unsigned long long value) noexcept
      {
        return wsl::size_t(value * 1000 * 1000);
      }

      constexpr wsl::size_t operator"" _GiB(unsigned long long value) noexcept
      {
        return wsl::size_t(value * 1024 * 1024 * 1024);
      }

      constexpr wsl::size_t operator"" _GB(unsigned long long value) noexcept
      {
        return wsl::size_t(value * 1000 * 1000 * 1000);
      }

    } // namespace literals
  }   // namespace memory
} // namespace wsl

#endif /*WSL_MEMORY_HXX*/