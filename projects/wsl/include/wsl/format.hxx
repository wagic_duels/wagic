#ifndef WSL_FORMAT_HXX
#define WSL_FORMAT_HXX

#include <fmt/format.h>
#include <fmt/printf.h>

// based on https://fmt.dev/Text%20Formatting.html
namespace wsl
{
    template<class... Args>
    inline std::string format(const std::string& fmt, const Args&... args)
    {
        return fmt::format(fmt, args...);
    }

    template<class... Args>
    inline std::string sprintf(const std::string& fmt, const Args&... args)
    {
        return fmt::sprintf(fmt, args...);
    }
}

#endif /*WSL_FORMAT_HXX*/