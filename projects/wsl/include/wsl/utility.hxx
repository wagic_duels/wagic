#ifndef WSL_UTILITY_HXX
#define WSL_UTILITY_HXX

#include <utility>

#include <wsl/type_traits.hxx>

namespace wsl
{

  template <class F>
  class final_action
  {
  public:
    explicit final_action(F f) noexcept : m_function(std::move(f)) {}

    final_action(final_action&& other) noexcept
        : m_function(std::move(other.m_function)), m_call_needed(other.m_call_needed)
    {
      other.m_call_needed = false;
    }

    final_action(const final_action&) = delete;
    final_action& operator=(const final_action&) = delete;
    final_action& operator=(final_action&&) = delete;

    ~final_action() noexcept
    {
      if (m_call_needed)
        m_function();
    }

  private:
    F m_function;
    bool m_call_needed{true};
  };

  template <class F>
  final_action<F> finally(const F& f) noexcept
  {
    return final_action<F>(f);
  }

  template <class F>
  final_action<F> finally(F&& f) noexcept
  {
    return final_action<F>(std::forward<F>(f));
  }
} // namespace wsl

#endif // WSL_UTILITY_HXX