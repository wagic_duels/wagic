#ifndef WSL_THREAD_HXX
#define WSL_THREAD_HXX

#if defined PSP
#include "thread/psp_thread.hxx"
#else

#include <thread>
#include <algorithm>

namespace wsl
{
  using std::thread;
  namespace this_thread = std::this_thread;

  static inline unsigned hardware_concurrency() noexcept 
  { 
    return std::max(std::thread::hardware_concurrency(), 1u); 
  }
} // namespace wsl

#endif // PSP
#endif // WSL_THREAD_HXX