#ifndef WSL_TIME_HXX
#define WSL_TIME_HXX

#include <chrono>

#include <wsl/types.hxx>

namespace wsl
{
  namespace time
  {
    using std::chrono::duration;
    using std::chrono::duration_cast;
    using std::chrono::time_point;

    using nanoseconds = duration<wsl::int64_t, std::nano>;
    using microseconds = duration<wsl::int64_t, std::micro>;
    using milliseconds = duration<wsl::int64_t, std::milli>;
    using seconds = duration<wsl::int64_t>;
    using minutes = duration<wsl::int64_t, std::ratio<60>>;
    using hours = duration<wsl::int64_t, std::ratio<3600>>;

    namespace literals
    {

      constexpr milliseconds operator""_ms(unsigned long long ms) noexcept { return milliseconds(ms); }
      constexpr duration<long double, std::milli> operator""_ms(long double ms) noexcept
      {
        return duration<long double, std::milli>(ms);
      }

      constexpr seconds operator""_s(unsigned long long s) noexcept { return seconds(s); }
      constexpr duration<long double> operator""_s(long double s) noexcept { return duration<long double>(s); }

    } // namespace literals
  }   // namespace time

} // namespace wsl

#endif // WSL_TIME_HXX