#ifndef WSL_MUTEX_HXX
#define WSL_MUTEX_HXX

#if defined(PSP)
#include <wsl/thread/psp_mutex.hxx>
#else

#include <mutex>

namespace wsl
{
  using std::mutex;
  using std::recursive_mutex;

  using std::lock_guard;
  using std::unique_lock;

  using std::adopt_lock_t;
  using std::lock;

} // namespace wsl

#endif

namespace wsl
{
  template <typename... MutexTypes>
  class scoped_lock
  {
    template <std::size_t I = 0>
    typename std::enable_if<I == sizeof...(MutexTypes), void>::type invoke_unlock()
    {
    }

    template <std::size_t I = 0>
        typename std::enable_if < I<sizeof...(MutexTypes), void>::type invoke_unlock()
    {
      std::get<I>(m_mutex_tuple).unlock();
      invoke_unlock<I + 1>();
    }

  public:
    explicit scoped_lock(MutexTypes&... mtx) : m_mutex_tuple(mtx...) { lock(mtx...); }
    explicit scoped_lock(adopt_lock_t, MutexTypes&... mtx) : m_mutex_tuple(mtx...) {}
    ~scoped_lock() { invoke_unlock<>(); }

    scoped_lock(const scoped_lock&) = delete;
    scoped_lock& operator=(const scoped_lock&) = delete;

  private:
    std::tuple<MutexTypes&...> m_mutex_tuple;
  };

  template <>
  class scoped_lock<>
  {
  public:
    explicit scoped_lock() = default;
    explicit scoped_lock(adopt_lock_t) {}
    ~scoped_lock() = default;

    scoped_lock(const scoped_lock&) = delete;
    scoped_lock& operator=(const scoped_lock&) = delete;
  };

  template <typename Mutex>
  class scoped_lock<Mutex>
  {
  public:
    using mutex_type = Mutex;

    explicit scoped_lock(Mutex& m) : m_mutex(m) { m.lock(); }
    explicit scoped_lock(adopt_lock_t, Mutex& m) : m_mutex(m) {}
    ~scoped_lock() { m_mutex.unlock(); }

    scoped_lock(const scoped_lock&) = delete;
    scoped_lock& operator=(const scoped_lock&) = delete;

  private:
    Mutex& m_mutex;
  };
} // namespace wsl

#endif // WSL_MUTEX_HXX