#include <wsl/format.hxx>

#include <catch.hpp>

TEST_CASE("test format functionality", "[wsl][format]")
{
  SECTION("simple format replacement")
  {
    const std::string target_string("I'd rather be happy than right.");
    const std::string s = wsl::format("I'd rather be {1} than {0}.", "right", "happy");
    REQUIRE_THAT(s, Catch::Matchers::Equals(target_string));
  }
}