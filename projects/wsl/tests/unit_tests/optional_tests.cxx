#include <wsl/optional.hxx>

#include <catch.hpp>

TEST_CASE("test basic functionality of optional", "[wsl][optional]")
{
  SECTION("default creation should end in an empty object")
  {
    wsl::optional<int> opt;
    REQUIRE_FALSE(opt);
  }

  SECTION("creation with value should be true")
  {
    wsl::optional<int> opt{2};
    REQUIRE(opt);
  }
}