#include <wsl/debug.hxx>

#include <malloc.h>

namespace wsl
{
  int get_complete_allocated_memory() noexcept
  {
    const auto malloc_info = mallinfo();
    return malloc_info.uordblks + malloc_info.hblkhd;
  }
} // namespace wsl