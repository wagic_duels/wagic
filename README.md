# Wagic the Homebrew
[![Build status](https://gitlab.com/wagic_duels/wagic/badges/master/pipeline.svg)](https://gitlab.com/wagic_duels/wagic/commits/master)
[![Build status](https://ci.appveyor.com/api/projects/status/ydpkvlalcgqi82mo/branch/master?svg=true)](https://ci.appveyor.com/project/zie8778791/wagic/branch/master)
[![Coverage Status](https://gitlab.com/wagic_duels/wagic/badges/master/coverage.svg)](https://gitlab.com/wagic_duels/wagic/commits/master)

## Description

This repository is a fork of the orignal [Wagic](https://github.com/WagicProject/wagic) sources. In the current state this Project is only a source code refactoring of the original version. For an overview of the changes take a look at the [modification list](./docs/modifications.md)

Wagic, the Homebrew, is a C++ game engine that allows to play card games against an AI on
- [Sony PSP](./docs/psp.md) (runs but only very slow)
- Android (not tested) 
- Windows desktops (currently no build available)
- Linux (X11, SDL, Qt) 

It is highly customizable and allows the player to tweak the rules / create their own cards, their own themes, etc... 


Info, downloads, discussions and more at http://wololo.net/forum/index.php

![alt text](http://wololo.net/wagic/wp-content/uploads/2009/10/shop.jpg "Screenshot")

### Sample round play-through video
[![IMAGE ALT TEXT HERE](http://img.youtube.com/vi/WUFSAPZuDIk/0.jpg)](http://www.youtube.com/watch?v=WUFSAPZuDIk)

## License

This is a short summary of the licenses of the various parts of _Wagic, The Homebrew_.
More specific License texts can be found in each subfolder. In particular,
resource files (graphics, etc...) do **not** follow the same licenses as the source code.

* JGE is licensed under the [BSD License](./projects/jge/LICENSE.md)
* Wagic's source code is licensed under the [BSD License](./projects/mtg/LICENSE.md)
* Wagic's resources (graphics, sounds, etc...) use [various licenses](./projects/mtg/Res/LICENSE)

## About the sources

This Project uses a lot of additional libraries which are developed or adapted by this project.

### JGE++ (Jas Game Engine++)

This project needs the JGE++ library. JGE++ has been discontinued, so the version included in the package has been modified to fit the needs of this project.

### HGE (Haaf's Game Engine)

Some parts of [HGE](https://kvakvs.github.io/hge/) are used in this project. They are included inside of _JGE_. The files are based on an older version of the original Engine and are modified.

### WSL (Wagic System Layer)

The Operating System abstraction inside of JGE is heavily Macro based. This approch isn't easy to handle in the current state and very error prune. _WSL_ should fix the problem with an clean and easy abstraction of the operating system differences (e.g. thread handling). In addition to this it will backport some _STL_ features to use with older compilers. 