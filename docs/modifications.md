# Modifications

This is an overview about modifications to the original [Wagic sources](https://github.com/WagicProject/wagic).

## Done

- [x] moved to gitlab
- [x] port [travis-ci](https://travis-ci.org/) to [gitlab-ci](https://docs.gitlab.com/ee/ci/)
- [x] change build system to [cmake](https://cmake.org/)
- [x] clean up dependency handling (centralize dependencies on a single place)
- [x] drop support for MacOS and iOS
- [x] drop Boost dependency
- [x] integrate wsl
    - [x] thread handling
    - [x] mutex
    - [x] smart pointer

## Tasks
- [ ] get the [*PSP*](./psp.md) build running again **WIP**
    - [x] reduce size of must created objects (see: [memory](./memory.md))
    - [ ] improve texture handling
    - [x] improve texture size
- [ ] centralize logging (currently 5 different kinds of logging are used) **WIP**
    - [x] create simple logging handler (cout, fstream)
    - [ ] replace old logging calls **WIP**
    - [ ] create efficient platform dependent logger
- [ ] modernize with C++11 **WIP**
    - [x] enable C++11 builds
    - [ ] enable move for internal objects
- [ ] integrate automated test (testsuite, integration tests and unit tests) 
    - [x] testsuite
    - [ ] integration tests
    - [ ] unit tests 
- [ ] extend wsl
    - [x] not_null ptr
    - [x] optional
    - [ ] string formatter
    - [ ] time handling
    - [ ] socket
- [ ] reenable windows support

## Maybe Tasks
- [ ] replace filesystem handling with physfs to get rid of own file handling
- [ ] simplify asset loading
- [ ] general audio handling (platform independent)
- [ ] database handling (real database not multiple files anymore)
- [ ] simplify parsing and scripting of cards (maybe use [ChaiScript](http://chaiscript.com/))