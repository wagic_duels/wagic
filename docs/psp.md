# Playstation Portable

Currently, the PSP build is running but very very slow (only between 1 and 2 fps). 
There were several steps to do to get it in this state again.

Done so far:
- [x] changed build system to [cmake](./modifications.md)
- [x] updated toolchain for PSP to new compiler to enable C++11
- [x] reduced the [object size](./memory.md)
- [x] [prescaled textures](./modifications.md)

Task to do:
- [ ] change texture handling (currently the system is slowed down by reloading)
- [ ] improve the overall memory handling (RAM and VRAM)
    - [ ] implement memory pool
    - [ ] change allocators to use memory pool
    - [ ] texture memory handling
    - [ ] font and text (string) handling (currently uses more then 4 MB)

## Flash PSP

[![IMAGE ALT TEXT HERE](http://img.youtube.com/vi/FZB7MQCeKkU/0.jpg)](https://www.youtube.com/watch?v=FZB7MQCeKkU)

## Memory Debugging

- [PSP and memory allocation](http://lukasz.dk/mirror/forums.ps2dev.org/viewtopic7216.html?t=5402)
- [Free memory available functions](http://lukasz.dk/mirror/forums.ps2dev.org/viewtopic8f6d-2.html?t=4784)
- [My Memory Findings](https://web.archive.org/web/20071215132307/http://www.psp-programming.com/forums/index.php?topic=2731.0)


## Technical Details

See [PSPTEK](http://uofw.github.io/upspd/docs/hardware/PSPTEK.htm) description.

