# Memory

There are some harsh Memory limits given by the *PSP* enviroment. Currently the *PSP* build doesn't work based on the current memory usage. To make the build running again we need to use the memory size of the basics and also improve the texture handling.

## Problems

Currently a lot of memory is used to store some of the basic functionality as strings (e.g. *magic_text*, *name*). This consums more than **3 MB** of RAM. This is to much for the *PSP*.

Anyhow if we get rid of all string we should have enough memory (only round about 12 MB is used). But the build will also crash if a game is started. It seems the loading of the textures itself overruns the memory limit.    

## Tasks

Following task are necessary to reduce the memory footprint:

- [x] reduce object size of importend objects (see [object statistics](./object_statistics.md))
- [ ] track usage of textures and assets
- [ ] centralize the allocations and refactoring of the resources management
- [ ] rework string usage
    - [ ] own string class with own allocator for easy memory tracking
    - [ ] restructure string commands and string ability parser
    - [ ] replace strings with flightweight objects for ability handling
- [ ] lazy loading (sqlite based and not text file based) **maybe** 
- [ ] allocation via specific [memory managers](./memory_managers.md) **maybe** 



