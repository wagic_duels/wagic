# Object statistics

The following statistics aren't the truth. They show only the differences of `sizeof`. A lot of the memory reducing is done with a map to hide optional member if not needed. By doing so only the map size is considered by `sizeof` not the pointer nodes it self. This means that worst case it uses now more memory than bevor but in the average it uses less. A huge problem is also that string contents aren't calculated.

## start memory usages of basic objects


| Linux     | Previous  | PSP   | Previous  |
|----------:|----------:|------:|-----------|
|56.6       |35.1       |16.7   |10.0       |                   


|                    |   Linux    |  Previous   |     PSP   | Previous  | Count     |
|:-------------------|-----------:|------------:|----------:|----------:|----------:|
|MTGCard             |         32 |32           |        20 |20         |29934      |
|MTGCardInstance     |      2,192 |1480         |     1,128 |752        |80         |
|CardPrimitive       |        688 |480          |       240 |184        |16008      |
|ManaCost            |        176 |160          |        80 |72         |17154      |
|ExtraCosts          |         40 |64           |        20 |20         |365        |
|                    |`15,180,456`|             |`5,910,460`|           |           |

## current memory usage


|Linux               |   Current  |  Previous   |    Diff    |
|:-------------------|-----------:|------------:|-----------:|
|MTGCard             |  32        |32           |   0        |
|MTGCardInstance     |  1976      |2192         |   -216     |
|CardPrimitive       |  520       |688          |   -168     |
|ManaCost            |  152       |176          |   -24      |
|ExtraCosts          |  72        |40           |   32       |
|                    |`12,073,816`|`15,180,456` |`-3,106,640`|

|PSP                 |     PSP   | Previous  |  Diff     |
|:-------------------|----------:|----------:| ---------:|
|MTGCard             |    20     |20         | 0         |
|MTGCardInstance     |    1088   |1128       | -40       |
|CardPrimitive       |    224    |236        | -12       |
|ManaCost            |    68     |80         | -12       |
|ExtraCosts          |    24     |20         | +4        |
|                    |`5,446,744`|`5,910,460`|`-463,716` |

## old vs new psp statistics

|                    | Count     |    Current  | WtH 0.18.6  |
|:-------------------|----------:|------------:|------------:|
|MTGCard             |29934      |     598,680 |     598,680 |
|MTGCardInstance     |80         |      87,040 |      60,160 |
|CardPrimitive       |16008      |   3,585,792 |   2,945,472 |
|ManaCost            |17154      |   1,166,472 |   1,235,088 |
|ExtraCosts          |365        |       8,760 |       7,300 |
|                    |           |  `5,446,744`|  `4,846,700`|


|                                      |WtH 0.18.6 | Current   |
|:-------------------------------------|----------:|----------:|
| CardPrimitive current count          |     9,458 |     16,008|
| MTGCard current count                |    16,507 |     29,934|
| MTGCardInstance current count        |        57 |         80|
| ManaCost current count               |    10,039 |     17,154|
| ExtraCost current count              |       259 |        365|
|                                      |           |           |
| CardPrimitive current byte usage     | 1,740,272 | 3,585,792 |
| MTGCard current byte usage           |   330,140 |   598,680 |
| MTGCardInstance current byte usage   |    42,864 |    87,040 |
| ManaCost current byte usage          |   722,808 | 1,166,472 |
| ExtraCost current byte usage         |     5,180 |     8,760 |
|                                      |`2,841,264`|`5,446,744`|
|                                      |           |           |
| CardPrimitive max count              |     9,458 |    16,008 |
| MTGCard max count                    |    16,507 |    29,934 |
| MTGCardInstance max count            |        57 |        82 |
| ManaCost max count                   |    10,039 |    17,156 |
| ExtraCost max count                  |       259 |       365 |
|                                      |           |           |
| CardPrimitive max byte usage         | 1,740,272 | 3,585,792 |
| MTGCard max byte usage               |   330,140 |   598,680 |
| MTGCardInstance max byte usage       |    42,864 |    89,216 |
| ManaCost max byte usage              |   722,808 | 1,166,608 |
| ExtraCost max byte usage             |     5,180 |     8,760 |