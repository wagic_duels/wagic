SET(CMAKE_SYSTEM_NAME Generic)
SET(CMAKE_SYSTEM_VERSION 1)
SET(CMAKE_CROSSCOMPILING TRUE)

##############################################################

set(TOOLCHAIN_PREFIX psp-)

execute_process(COMMAND psp-config --pspsdk-path
  OUTPUT_VARIABLE PSPSDK_PATH
  OUTPUT_STRIP_TRAILING_WHITESPACE)

execute_process(COMMAND psp-config --psp-prefix
  OUTPUT_VARIABLE PSP_PREFIX
  OUTPUT_STRIP_TRAILING_WHITESPACE)

get_filename_component(PSP_TOOLCHAIN_DIR ${PSPSDK_PATH} DIRECTORY)


set(PRXSPECS_FILE "${PSPSDK_PATH}/lib/prxspecs")
set(LINKFILE_FILE "${PSPSDK_PATH}/lib/linkfile.prx")

# Without that flag CMake is not able to pass test compilation check

SET(BUILD_SHARED_LIBS FALSE)
set(CMAKE_TRY_COMPILE_TARGET_TYPE STATIC_LIBRARY)
set(CMAKE_EXE_LINKER_FLAGS_INIT "--specs=${PRXSPECS_FILE} -Wl,-q,-T${LINKFILE_FILE}")


set(CMAKE_C_COMPILER ${TOOLCHAIN_PREFIX}gcc)
set(CMAKE_ASM_COMPILER ${CMAKE_C_COMPILER})
set(CMAKE_CXX_COMPILER ${TOOLCHAIN_PREFIX}g++)

set(CMAKE_OBJCOPY ${PSP_TOOLCHAIN_DIR}/${TOOLCHAIN_PREFIX}objcopy CACHE INTERNAL "objcopy tool")
set(CMAKE_SIZE_UTIL ${PSP_TOOLCHAIN_DIR}/${TOOLCHAIN_PREFIX}size CACHE INTERNAL "size tool")

# set(CMAKE_SYSROOT ${PSP_TOOLCHAIN_DIR})
set(CMAKE_FIND_ROOT_PATH "${PSPSDK_PATH};${PSP_PREFIX}")
set(CMAKE_FIND_ROOT_PATH_MODE_PROGRAM NEVER)
set(CMAKE_FIND_ROOT_PATH_MODE_LIBRARY ONLY)
set(CMAKE_FIND_ROOT_PATH_MODE_INCLUDE ONLY)

link_directories("${PSPSDK_PATH}/lib;${PSP_PREFIX}/lib")
include_directories(SYSTEM "${PSPSDK_PATH}/include;${PSP_PREFIX}/include")

# SET(CMAKE_EXECUTABLE_SUFFIX ".elf")

find_program(PSPSDK_FIXUP_IMPORTS_COMMAND psp-fixup-imports)
find_program(PSPSDK_PRXGEN_COMMAND psp-prxgen)
find_program(PSPSDK_PACK_PBP_COMMAND pack-pbp)
find_program(PSPSDK_MKSFO_COMMAND mksfo)


################################################################

# reduce link error
ADD_DEFINITIONS("-G0 -DPSP")

# linker flags
set(PSP_LIBRARIES "-lpspdebug -lpspdisplay -lpspge -lpspctrl -lc -lpspsdk -lc -lpspnet -lpspnet_inet -lpspnet_apctl -lpspnet_resolver -lpspaudiolib -lpsputility -lpspuser -lpspkernel")
set(CMAKE_C_STANDARD_LIBRARIES "${PSP_LIBRARIES}")
set(CMAKE_CXX_STANDARD_LIBRARIES "-lstdc++ ${PSP_LIBRARIES}")

set(PSP ON)
